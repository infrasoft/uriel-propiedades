-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-08-2017 a las 23:44:38
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `urieldb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `word` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `idProp` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `comentario` text COLLATE utf8_spanish2_ci,
  `estado` enum('publicado','no_publicado') COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`idProp`, `idUser`, `fecha`, `hora`, `comentario`, `estado`) VALUES
(1, 1, '2017-01-11', '21:08:23', '324234', 'publicado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mjes`
--

CREATE TABLE `mjes` (
  `id_mensaje` int(11) NOT NULL,
  `hora` time DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `asunto` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cuerpo` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `est` enum('') COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL,
  `Apellido` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Nombre` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_face` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pass` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `barrio` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `celular` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipoDoc` enum('DNI','Pasaporte','Libreta Civica','Otros','Visa') COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cuil` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sexo` enum('hombre','mujer','otro') COLLATE utf8_spanish_ci DEFAULT NULL COMMENT '1: varon 2: mujer',
  `fechaNac` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `propiedad`
--

CREATE TABLE `propiedad` (
  `idProp` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT ' ',
  `descripcion` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `provincia` enum('Salta') COLLATE utf8_spanish_ci DEFAULT 'Salta',
  `localidad` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo` enum('casa','departamento','terreno','local','quinta','galpon','cochera','quinta') COLLATE utf8_spanish_ci DEFAULT NULL,
  `requisitos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `operacion` enum('venta','alquiler','tasacion','administracion') COLLATE utf8_spanish_ci DEFAULT NULL,
  `documentacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `planos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `agua` enum('si','no') COLLATE utf8_spanish_ci DEFAULT NULL,
  `luz` enum('si','no') COLLATE utf8_spanish_ci DEFAULT 'si',
  `gaz` enum('si','no') COLLATE utf8_spanish_ci DEFAULT NULL,
  `cloacas` enum('si','no') COLLATE utf8_spanish_ci DEFAULT NULL,
  `asfalto` enum('si','no') COLLATE utf8_spanish_ci DEFAULT NULL,
  `banner` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `portada` enum('si','no') COLLATE utf8_spanish_ci DEFAULT 'si',
  `imagenes1` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagenes2` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagenes3` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagenes4` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagenes5` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mapa` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sup_construida` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sup_semidescubierta` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sup_descubierta` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sup_total` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` enum('abierta','reservada','cerrada') COLLATE utf8_spanish_ci DEFAULT NULL,
  `otros` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `propiedad`
--

INSERT INTO `propiedad` (`idProp`, `nombre`, `descripcion`, `provincia`, `localidad`, `tipo`, `requisitos`, `operacion`, `documentacion`, `direccion`, `precio`, `planos`, `agua`, `luz`, `gaz`, `cloacas`, `asfalto`, `banner`, `portada`, `imagenes1`, `imagenes2`, `imagenes3`, `imagenes4`, `imagenes5`, `mapa`, `sup_construida`, `sup_semidescubierta`, `sup_descubierta`, `sup_total`, `estado`, `otros`) VALUES
(1, 'Venta de edificio en pleno centro de Salta', 'Edificio con 4 locales, 5 oficinas y una casa con 2 dormitorios, 2 baños, gimnacio, asador, terraza', 'Salta', 'Salta', '', 'Operacion Flexible', 'venta', 'Completa', 'Alvarado 1073', 27000000, 'Si', 'si', 'si', 'si', 'si', 'si', 'edi10.jpg', 'si', 'edi1.jpg', 'edi2.jpg', 'edi3.jpg', 'edi7.jpg', 'edi9.jpg', 'https://www.google.com/maps/d/viewer?mid=1nZ4_PPxD', '600', '200', '100', '900', 'abierta', '			 '),
(2, 'Vendo IMPORTANTE PROPIEDAD en Campo Quijano.', 'Vendo IMPORTANTE PROPIEDAD en Campo Quijano. 1 Ha. Con casa principal de PRIMERA con todas las comodidades, y casa para deposito o personal servicio. ¡¡¡ Excelente Ubicación !!!. A 50 mts. de ruta 51 . Totalmente parquizado. Tranquera. Portón corredizo de desde el interior. Paneles solares y energía eléctrica. Chancha de gas. Cochera doble techada. Sala de estar. 2 dormitorios. 2 baños. Sala de estar comedor 4 x 10 mts. Mesada cocina completa 0.70 x 7 mts lineales. Isla central en cocina. Bacha doble. Lavadero. Galeria 3 x 13 mts', 'Salta', 'Campo Quijano', 'casa', 'Operacion Flexible', 'venta', 'completa', 'A 50 mts. de ruta 51', 9000000, 'si', 'si', 'si', 'si', 'no', 'no', 'DSC08338.jpg', 'si', 'DSC08329.jpg', 'DSC08330.jpg', 'DSC08335.jpg', 'DSC08343.jpg', 'DSC08344.jpg', '', '', '', '', '10000', 'abierta', '			 '),
(3, 'Vendo  Casa en Campo Quijano', '507 mt2. 40 mt2 de ruta a Rosario de Lerma y 150 mt de ruta 51 a Salta.', 'Salta', 'Campo Quijano', 'casa', 'Operacion Flexible', 'venta', 'Completa', 'A 150 mts. de ruta 51', 1200000, '', 'si', 'si', 'no', 'si', 'no', '2.jpg', 'si', '6.jpg', '2.jpg', '10.jpg', '1.jpg', '', '', '', '', '', '507', 'abierta', '			https://www.facebook.com/pg/UrielPropiedades/photos/?tab=album&album_id=1319032281484706 '),
(4, 'Vendo Terreno en Barrio San Lucas', '300 mt2. (10x 30) Cercado. Servicio de luz y agua', 'Salta', 'Salta', 'terreno', 'Operacion Flexible', 'venta', 'Boleto Compra venta', 'Bª San Lucas', 200000, '', 'si', 'si', 'no', 'no', 'no', 'IMG_20160903_125122535_HDR.jpg', 'si', 'IMG_20160903_125020507_HDR.jpg', 'IMG_20160903_125132625_HDR.jpg', 'IMG_20160903_125038756_HDR.jpg', 'IMG_20160903_125057153_HDR.jpg', 'IMG_20160903_125117910.jpg', '', '', '', '300', '300', 'abierta', '			 '),
(5, 'Vendo 2 terrenos Juntos en Jardines de Quijano', 'Se encuentran a 3 kms de Campo Quijano y a 40 mts de la ruta. 1000 mt2', 'Salta', 'Campo Quijano', 'terreno', 'Operacion Flexible', 'venta', 'Completa', 'Jardines de Quijano', 270000, '', 'si', 'si', 'no', 'no', 'no', '3.jpg', 'si', '1.jpg', '2.jpg', '3.jpg', '', '', '', '', '', '1000', '1000', 'abierta', '			 https://www.facebook.com/pg/UrielPropiedades/photos/?tab=album&album_id=1418400064881260');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `us`
--

CREATE TABLE `us` (
  `user` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `us`
--

INSERT INTO `us` (`user`, `pass`) VALUES
('proyect', 'amd159159');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idProp`,`idUser`,`hora`,`fecha`);

--
-- Indices de la tabla `mjes`
--
ALTER TABLE `mjes`
  ADD PRIMARY KEY (`id_mensaje`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`),
  ADD UNIQUE KEY `id_face` (`id_face`);

--
-- Indices de la tabla `propiedad`
--
ALTER TABLE `propiedad`
  ADD PRIMARY KEY (`idProp`);

--
-- Indices de la tabla `us`
--
ALTER TABLE `us`
  ADD PRIMARY KEY (`user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `mjes`
--
ALTER TABLE `mjes`
  MODIFY `id_mensaje` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `propiedad`
--
ALTER TABLE `propiedad`
  MODIFY `idProp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

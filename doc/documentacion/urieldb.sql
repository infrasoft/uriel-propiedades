-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-01-2017 a las 22:50:44
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `urieldb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `idProp` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `comentario` text COLLATE utf8_spanish2_ci,
  `estado` enum('publicado','no_publicado') COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`idProp`, `idUser`, `fecha`, `hora`, `comentario`, `estado`) VALUES
(1, 1, '2017-01-11', '21:08:23', '324234', 'publicado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mjes`
--

CREATE TABLE `mjes` (
  `id_mensaje` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `cuerpo` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `est` enum('') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `asunto` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL,
  `Apellido` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Nombre` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_face` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pass` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `barrio` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `celular` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipoDoc` enum('DNI','Pasaporte','Libreta Civica','Otros','Visa') COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cuil` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sexo` enum('hombre','mujer','otro') COLLATE utf8_spanish_ci DEFAULT NULL COMMENT '1: varon 2: mujer',
  `fechaNac` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `propiedad`
--

CREATE TABLE `propiedad` (
  `idProp` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT ' ',
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `provincia` enum('Salta') COLLATE utf8_spanish_ci DEFAULT 'Salta',
  `localidad` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo` enum('casa','departamento','terreno','local','quinta','galpon','cochera','quinta') COLLATE utf8_spanish_ci DEFAULT NULL,
  `requisitos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `operacion` enum('venta','alquiler','tasacion','administracion') COLLATE utf8_spanish_ci DEFAULT NULL,
  `documentacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `planos` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `agua` enum('si','no') COLLATE utf8_spanish_ci DEFAULT NULL,
  `luz` enum('si','no') COLLATE utf8_spanish_ci DEFAULT 'si',
  `gaz` enum('si','no') COLLATE utf8_spanish_ci DEFAULT NULL,
  `cloacas` enum('si','no') COLLATE utf8_spanish_ci DEFAULT NULL,
  `asfalto` enum('si','no') COLLATE utf8_spanish_ci DEFAULT NULL,
  `banner` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `portada` enum('si','no') COLLATE utf8_spanish_ci DEFAULT 'si',
  `imagenes1` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagenes2` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagenes3` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagenes4` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagenes5` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mapa` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sup_construida` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sup_semidescubierta` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sup_descubierta` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sup_total` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` enum('abierta','reservada','cerrada') COLLATE utf8_spanish_ci DEFAULT NULL,
  `otros` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idProp`,`idUser`,`hora`,`fecha`);

--
-- Indices de la tabla `mjes`
--
ALTER TABLE `mjes`
  ADD PRIMARY KEY (`id_mensaje`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`),
  ADD UNIQUE KEY `id_face` (`id_face`);

--
-- Indices de la tabla `propiedad`
--
ALTER TABLE `propiedad`
  ADD PRIMARY KEY (`idProp`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `propiedad`
--
ALTER TABLE `propiedad`
  MODIFY `idProp` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

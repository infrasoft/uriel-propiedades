<?php
/* 
captcha.php
# Script de publicacion de anuncios
#Autor : Ariel Marcelo Diaz
#Sitio Web: http://www.infrasoft.com.ar
# Licencia : GPL2 o Superiores

*/
session_start();
function randomText($length) {
    $pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $key= $pattern{rand(0,59)};   
    for($i=0;$i<$length-1;$i++) 
    {
      $key .= $pattern{rand(0,59)};
    }
    return $key;
}

$_SESSION['tmptxt'] = randomText(4);
$var = $_SESSION['tmptxt']; 
$captcha = imagecreatefromgif("images/bgcaptcha.gif");
$colText = imagecolorallocate($captcha, rand(0,170), rand(0,170), rand(0,170));
imagestring($captcha, 5, 16, 7, $_SESSION['tmptxt'], $colText);

header("Content-type: image/gif");
imagegif($captcha);

?>
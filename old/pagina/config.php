<?php
/*
config.php 
# Sitio web de Uriel Propiedades
#Autor : Ariel Marcelo Diaz
#Sitio Web: http://www.infrasoft.com.ar
# Licencia : GPL2 o Superiores  
*/
include("lib/libHtml.php");
include("lib/propiedades.php");
include("lib/consultasSQL.php");
header('Content-Type:text/html;charset=iso-8859-1');

// Pone la cabecera de la pagina con los meta y descripcion
function cabecera($key,$descripcion, $otros)
{
    echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta name='keywords' content='$key' />
<meta name='description' content='$descripcion' />
<meta http-equiv='content-type' content='text/html; charset=UTF-8' />
<title>Uriel Propiedades</title>
<link href='estilos/style.css' rel='stylesheet' type='text/css' media='screen' />
<script language='javascript' src='script/swfobject.js'></script>
<script language='javascript' src='script/intercambioImagenes.js'></script>
<script language='javascript' src='script/formularios.js'></script>
<link rel='shortcut icon' type='image/x-icon' href='images/fondo.ico'>
$otros
<script type='text/javascript'>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38758663-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>";
}

//muestra el menu principal del sitio
function muestraMenu()
{
    print "<body onload='alternar_banner()'>
<div id='wrapper'>	
		<div id='menu'>
            <ul> <li><a href='index.php'>Inicio</a></li>
            <li><a href='quienes.php'>Quienes Somos</a></li>            
            <li><a href='servicios.php'>Alquileres y ventas</a></li>
            <li><a href='contacto.php'>Contacto</a></li>		
            
			</ul>
		</div>
		<!-- end #menu -->

	</div>
	<!-- end #header -->";
}

function inicioPagina()
{
	print "<!-- end #menu -->
            <div id='header'>
			".genLink(
                muestraImagen("images/portada/img03.jpg",
                               "bannerPrincipal",
                               "name='bannerPrincipal' alt='Banner Principal'"),
                      "index.php","")
            ."</div>  <!-- end #header -->
			<div id='page'>
			<div id='page-bgtop'>
			<div id='page-bgbtm'>
			<div id='content'>";
}

//muestra un nuevo post
function post($titulo, $contenido)
{
	print "<div class='post'>
				<h2 class='title'>$titulo</h2>
								
				<div style='clear: both;'>&nbsp;</div>
				<div class='entry'>
					$contenido
				</div>
			</div>";
}

// coloca la columna derecha
function colDerecha($lista)
{
	print "<div style='clear: both;'>&nbsp;</div>
		</div>
		<!-- end #content -->
		<div id='sidebar'>".genLista($lista, "sidebar");
}

function piePagina()
{
	print "<script async src='//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'></script>
<!-- uriel propiedades -->
<ins class='adsbygoogle'
     style='display:inline-block;width:728px;height:90px'
     data-ad-client='ca-pub-4988234002472046'
     data-ad-slot='4437448016'></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
		<!-- end #sidebar -->
		<div style='clear: both;'>&nbsp;</div>
	</div>
	</div>
	</div>
	<!-- end #page -->
	
</div>
	<div id='footer'>
		<p><a href='http://infrasoft.com.ar'>Infrasoft - Servicios informaticos - http://www.infrasoft.com.ar</a></p>
	</div>
	<!-- end #footer -->
</body>
</html>";
}

// muestra el error del envio de mail
function errorMail($error)
{ 
	switch ($error)
  {
    case 1:
      return "<b>Mail enviado Correctamente</b>";    
    break;
    case 2:
      return "<b>Fallo en el envio de mail. Intentelo nuevamente</b>";
    break;
    case 3:
      return "<b>Codigo no ingresado correctamente</b>";
    	break;
    default:
 	    return "";
 	  break;
 }
}

$conexion = conectarbase();
?>

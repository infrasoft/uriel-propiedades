<?php
/*
plantilla.php  Plantilla
# Sitio web de Uriel Propiedades
#Autor : Ariel Marcelo Diaz
#Sitio Web: http://www.infrasoft.com.ar
# Licencia : GPL2 o Superiores  
*/

header('Content-Type: text/html; charset=utf-8');
include("config.php");
$conexion = conectarbase();
	if(isset($_GET["pos"]))
	{
		$pos = $_GET["pos"];
	}
	else
	{
		$pos = 0;
	}
	
	if(isset($_GET["id"]))
	{
		$id = $_GET["id"];
	}
	else
	{
		$id = 0;
	}
	
	if(isset($_GET["tipo"]))
	{
		$tipo = $_GET["tipo"];
	}
	else
	{
		$tipo = 0;
	}
	$mensaje = "";
	$mensaje = mensajePropiedad($conexion, $id);
    
    /* datos de formulario de contacto*/
    $contenido = array();
    if(isset($_GET["nombre"]))
	{
		$contenido['nombre'] = $_GET["nombre"];
	}
	else
	{
		$contenido['nombre'] = "";
	}
	
	if(isset($_GET["apellido"]))
	{
		$contenido['apellido'] = $_GET["apellido"];
	}
	else
	{
		$contenido['apellido'] = "";
	}
	
	if(isset($_GET["domicilio"]))
	{
		$contenido['domicilio'] = $_GET["domicilio"];
	}
	else
	{
		$contenido['domicilio'] = "";
	}
    
    if(isset($_GET["telefono"]))
	{
		$contenido['telefono'] = $_GET["telefono"];
	}
	else
	{
		$contenido['telefono'] = "";
	}
    
    if(isset($_GET["celular"]))
	{
		$contenido['celular'] = $_GET["celular"];
	}
	else
	{
		$contenido['celular'] = "";
	}
    
    if(isset($_GET["mail"]))
	{
		$contenido['mail'] = $_GET["mail"];
	}
	else
	{
		$contenido['mail'] = "";
	}
	
	if(isset($_GET["asunto"]))
	{
		$contenido['asunto'] = $_GET["asunto"];
	}
	else
	{
		$contenido['asunto'] = "";
	}
	
	if(isset($_GET["comentarios"]))
	{
		$contenido['comentarios'] = $_GET["comentarios"];
	}
	else
	{
		$contenido['comentarios'] = "";
	}
    
    session_start();
cabecera("", "", "");

muestraMenu();
inicioPagina();

	post("Contacto", "<p>Ponemos a su disposición un formulario de contacto,
	para comunicarse con nosotros </p>
	<div align='center'>".errorMail($pos).
	generaformulario(array(
		array("(*)Nombre:",genImput("nombre", 35, $contenido['nombre'], "edt", 50)),
		array("Apellido:",genImput("apellido", 20, $contenido['apellido'], "edt", 50)),
		array("Domicilio", genImput("domicilio", 30, $contenido['domicilio'], "edt", 50)),
		array("Telefono:",genImput("telefono", 15, $contenido['telefono'], "edt", 50)),
		array("(*)Celular:",genImput("celular", 15, $contenido['celular'], "edt", 50)),
		array("(*)Mail:",genImput("mail", 20, $contenido['mail'], "edt", 50)),        
		array("(*)Asunto",genImput("asunto", 25, $contenido['asunto'], "edt", 50)),
		array("(*)Comentarios:",generaText("comentarios", 27, 8, "$mensaje".$contenido['comentarios'], "edt")),
        array(muestraImagen("captcha.php", "captcha", "class='edit' name='captcha'"),
               "Verificar:".genImput("tmptxt", 10, "", "edt", 10).
               "<a href ='javascript: void actucap();' onclick='javascript:void actucap();'>Actualizar</a>"
               ),
        
	), "contacto", "lib/mail.php", "contacto", "onsubmit='return validarContacto(this)'").
    "<p>Alguno de los campos son obligatorios(*)</p></div>");
	
	colDerecha(array(generaBannerHorizontal($conexion, 2, "")));
	
	piePagina();
?>

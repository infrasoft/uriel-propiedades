<?php
/* 
consultasSQL.php
# Sitio web de Infrasoft.com.ar
#Autor : Ariel Marcelo Diaz
#Sitio Web: http://www.infrasoft.com.ar
# Licencia : GPL2 o Superiores
*/

	// conecta a la base de datos
    function conectarbase() 
    {
        $dbhost = "localhost";
        $dbusername = "root"; 
        $dbpass = "";
        $dbname = "urielbd";       
        // conectar con el servidor 
        $conexion = mysql_connect($dbhost,$dbusername,$dbpass) 
            or die("No se puede conectar al servidor");	
        // Seleccionar la bd
        mysql_select_db($dbname)
            or die ("No se puede seleccionar la base de datos"); 
        return $conexion;        
    }

	//Realiza una consulta basica
    function consulSQLbasica($conexion, $tabla, $campos, $condicion)
    {
        if($condicion == "" )
        {
            $query = "SELECT $campos FROM $tabla;";            
        }
        else
        {
            $query = "SELECT $campos FROM $tabla WHERE $condicion;";            
        }
        //print $query; 
        $consulta = mysql_query($query, $conexion)
            or die("Fallo en la consulta");
        return $consulta;
    }
	
	// Realiza una consulta basica y devuelve el array
    function consultaSQLbasicaRow($conexion, $tabla, $campos, $condicion)
    {
        $consulta = consulSQLbasica($conexion, $tabla, $campos, $condicion);
        $row = mysql_fetch_array($consulta);
        return $row;
    }
	
	// Realiza la modificacion de datos
    function modificarSQL($conexion, $tabla, $campos, $valores, $condicion)
    {
        $query = "UPDATE $tabla SET ";
        $atributos = explode(",", $campos);
        $val = explode(",", $valores);
        $j = 0;
        foreach($atributos as $i)
        {
            $query .= "$i = $val[$j],";
            $j++;
        }
        $query = substr($query, 0, strlen($query)-1);
        if($condicion != "")
        {
            $query .= " WHERE ";
            $query .= $condicion;
        }
        $query .= ";";        
        //print $query;
        $consulta = mysql_query($query, $conexion)
            or die("Fallo en la consulta");
        return $consulta;
    }
    
    // Muestra un select de una tabla predeterminada
    function selectTablaSQL($conexion, $tabla, $id, $muestra, $condicion, $predeterminado, $estilo)
    {
		$cod = "";
        $cod .= "<select name='$tabla' class='$estilo'>\n";
        $consulta = consulSQLbasica($conexion, $tabla, "$id, $muestra", $condicion);
		$aux = explode(",",$muestra);
		
        $row = mysql_fetch_array($consulta);
        while($row != null)
        {
			if($id == $predeterminado)
			{
				$cod .= "<option value='$row[$id]' selected='selected'>";
				foreach($aux as $i)
				{
					$cod .= "$row[$i] ";
				}
				$cod .= "</option>\n";
			}
			else
			{
				$cod .= "<option value='$row[$id]'> ";
				foreach($aux as $i)
				{                    
					$cod .= "$row[$i] ";
				}
				$cod .= "</option>\n";
			}
            $row = mysql_fetch_array($consulta);
        }
        $cod .= "</select>\n";
		return $cod;
    }
?>
<?php
/*************************************
 *****Club Deportivo Gran Bourg********
 ********Sitio Web****************
 
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
*Sitio Web: http://www.infrasoft.com.ar 
****************************************/ 

	//Genera un select para el armado de archivos
	function genSelect($nombre, $vector, $estilo)
	{
        $cod = "";
		$cod .= "<select name='$nombre' class='$estilo'>\n";
		foreach ($vector as $i)
		{
			$cod .= "<option value='$i'>$i</option>\n";
		}
		$cod .= "</select>\n";
        return $cod;
	}
	
    //Genera una lista 
	function genLista($vector, $estilo)
	{
        $cod = "";
        if($estilo =="")
        {
            $cod .= "<ul>\n";
        }
        else
        {
            $cod .= "<ul class='$estilo'>\n";
        }
		
		foreach($vector as $i)
        {
            $cod .= "<li>$i</li>\n";
        }
        $cod .= "</ul>";
		return $cod;
	}
    
    //genera link, devolviendo el valor 
    function genLink($nombre,$link,$estilo)
    {
        $devolver = "";
        if($estilo =="")
        {
            $devolver .= "<a href='$link'>$nombre</a>\n";
        }
        else
        {
            $devolver .= "<a href='$link' id='$estilo'>$nombre</a>\n";
        }
        return $devolver;
    }
    
    //muestra en pantalla un mensaje
    function muestraMjes($etiqueta, $mje, $estilo)
    {
    
        if($estilo =="")
        {
            print "<$etiqueta>$mje</$etiqueta>\n";
        }
        else
        {
            print "<$etiqueta id='$estilo'>$mje</$etiqueta>\n";
        }        
    }
    
    //muestra en pantalla un mensaje, devolviendo el valor
    function muestraMjes2($etiqueta, $mje, $estilo)
    {
    
        if($estilo =="")
        {
            return "<$etiqueta>$mje</$etiqueta>\n";
        }
        else
        {
            return "<$etiqueta id='$estilo'>$mje</$etiqueta>\n";
        }        
    }
    
    //genera tabla
    function genTabla($vector, $nombreTab, $otrosDatos)
    {
        $cod = "";
        if($nombreTab =="")
        {
            $cod .= "<table $otrosDatos>\n";
        }
        else
        {
            $cod .= "<table id='$nombreTab' $otrosDatos>\n";
        }
        
        foreach($vector as $i)
        {
            $cod .= "<tr>\n";
            foreach($i as $j)
            {
                $cod .= "<td>$j</td>\n";
            }
            $cod .= "</tr>\n";
        }
        $cod .= "</table>\n";
        return $cod;
    }
    
    //muestra una imagen
    function muestraImagen($url, $estilo, $otros)
    {
        $cod = "";
        if($estilo =="")
        {
            $cod .= "<img src='$url' $otros/>";
        }
        else
        {
            $cod .= "<img src='$url' id='$estilo' $otros/>";
        }
        return $cod;
    } 
       
    // genera un imput de un formulario
    function genImput($nombre, $size, $value, $estilo, $max)
    { // BEGIN function genImput
      $size = $size."px";
      if ($max == "")
      {
      	  $cod = "<input name='$nombre' type='text' size='$size' value='$value' class='$estilo'/>\n";
      }
      else 
      {
          $max = $max."px";
      	  $cod = "<input name='$nombre' type='text' size='$size' maxlength='$max' value='$value' class='$estilo'/>\n";
      }
    	return $cod;
    } // END function genImput
	
	// genera los text de un formulario
    function generaText($nombre, $col, $row, $valor, $estilo)
    {
        return "<textarea name='$nombre' cols='$col' rows='$row'  class='$estilo'>$valor</textarea>";
    }
    
    // Esto es para generar botones para formularios
    function genBoton($nombre, $size, $value, $estilo, $max, $tipo)
    { // BEGIN function genBoton
      	$cod = "";
      	if( $tipo==1)
      	{
          $tipo = "submit";
        }
      	else
      	{
          $tipo = "reset";
        }
      	$size = $size."px";
        if ($max == "")
        {
      	  $cod = "<input name='$nombre' type='$tipo' size='$size' value='$value' class='$estilo'/>\n";
        }
         else 
        {
          $max = $max."px";
      	  $cod = "<input name='$nombre' type='$tipo' size='$size' maxlength='$max' value='$value' class='$estilo'/>\n";
        }
      	return $cod;    	
    } // END function genBoton
    
    // genera el formulario 
    function generaformulario($vector, $nombreTab, $action, $nameForm, $otrosDatos)
    {
        $formulario = "<form name='$nameForm' id='$nameForm' action='$action' $otrosDatos>\n";                 
        $vector[] = array (genBoton("reset", "20px", "reset", "btn", "", 0), 
                            genBoton("enviar", "20px", "enviar", "btn", "", 1));        
        $formulario .= genTabla($vector, "$nombreTab","");
        $formulario .= "</form>\n";
        return $formulario;
    }
?>
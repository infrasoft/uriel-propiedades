<?php
/*************************************
 ***** Uriel Propiedades ********
 ********Sitio Web****************
 
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
*Sitio Web: http://www.infrasoft.com.ar 
****************************************/ 

// Librerias Incluidas
include("../config.php");

session_start();

    $contenido = array();
  if(isset($_GET["nombre"]))
	{
		$contenido['nombre'] = $_GET["nombre"];
	}
	else
	{
		$contenido['nombre'] = "";
	}
	
	if(isset($_GET["apellido"]))
	{
		$contenido['apellido'] = $_GET["apellido"];
	}
	else
	{
		$contenido['apellido'] = "";
	}
	
	if(isset($_GET["domicilio"]))
	{
		$contenido['domicilio'] = $_GET["domicilio"];
	}
	else
	{
		$contenido['domicilio'] = "";
	}
    
    if(isset($_GET["telefono"]))
	{
		$contenido['telefono'] = $_GET["telefono"];
	}
	else
	{
		$contenido['telefono'] = "";
	}
    
    if(isset($_GET["celular"]))
	{
		$contenido['celular'] = $_GET["celular"];
	}
	else
	{
		$contenido['celular'] = "";
	}
    
    if(isset($_GET["mail"]))
	{
		$contenido['mail'] = $_GET["mail"];
	}
	else
	{
		$contenido['mail'] = "";
	}
	
	if(isset($_GET["asunto"]))
	{
		$contenido['asunto'] = $_GET["asunto"];
	}
	else
	{
		$contenido['asunto'] = "";
	}
	
	if(isset($_GET["comentarios"]))
	{
		$contenido['comentarios'] = $_GET["comentarios"];
	}
	else
	{
		$contenido['comentarios'] = "";
	}
	
	if(isset($_GET["tmptxt"]))
	{
		$verificar = $_GET["tmptxt"];
	}
	else
	{
		$verificar = "";
	}

    
// realiza el envio de mail
function envioMail($vector)
{ // BEGIN function envioMail
  
  $contenido = "Apellido: $vector[apellido]<br/>\n
                Nombre: $vector[nombre]<br/>\n
                Domicilio: $vector[domicilio]<br/>\n
                Telefono: $vector[telefono]<br/>\n
                Celular:  $vector[celular]<br/>\n              
                Email: $vector[mail]<br/>\n
               Asunto: $vector[asunto]<br/>\n
               Contenido: $vector[comentarios]<br/>\n";
  $enviado=FALSE;
        $header = "";
        //para el envío en formato HTML
        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        //dirección de respuesta, si queremos que sea distinta que la del remitente
        $headers .= "Reply-To: $vector[mail]\r\n";
        
        //dirección del remitente
        $headers .= "From: $vector[mail] \r\n";
        
        //direcciones que recibián copia
        $headers .= "Cc:\r\n";
        
        //direcciones que recibirán copia oculta
        $headers .= "Bcc: marcelodiaz96@gmail.com\r\n";
        $enviado = mail("marcelodiaz96@gmail.com", $vector['asunto'], $contenido, $headers);	
        if($enviado)
        {
            echo 'Correo enviado';
            Header("Location: ../contacto.php?pos=1");
        }
        else
        {
            echo 'Correo no enviado';
            Header("Location: ../contacto.php?pos=2&apellido=$vector[apellido]&nombre=$vector[nombre]&domicilio=$vector[domicilio]&telefono=$vector[telefono]&mail=$vector[mail]&asunto=$vector[asunto]&comentarios=$vector[comentarios]");
        }
} // END function envioMail

  if ($_SESSION["tmptxt"] == $verificar)
  {
  	muestraMjes("h2", "Enviando el Email", "");
  	envioMail($contenido);
  }
  else
  {
    muestraMjes("h2", "Has ingresado un codigo incorrecto", "");
    Header("Location: ../contacto.php?pos=3&apellido=$vector[apellido]&nombre=$vector[nombre]&domicilio=$vector[domicilio]&telefono=$vector[telefono]&mail=$vector[mail]&asunto=$vector[asunto]&comentarios=$vector[comentarios]");   	
  }
?>

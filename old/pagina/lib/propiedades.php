<?php
/*
Libreria de publicacion de propiedades
# Sitio web de Uriel Propiedades
#Autor : Ariel Marcelo Diaz
#Sitio Web: http://www.infrasoft.com.ar
# Licencia : GPL2 o Superiores  
*/

//genera una galeria de fotos de manera automatica
function galeriaFotos($galeriaxml)
{
	return "<div id='flashcontent'>Se requiere Flash y JavaScript.
				<a href='http://www.macromedia.com/go/getflashplayer/'>
					Descargar Flash aqui.
				</a>
		    </div>	

        <script type='text/javascript'>
			var fo = new SWFObject('autoviewer.swf', 'album', '100%', '100%', '8', '#FFFFFF');		
				
			//Optional Configuration
			fo.addVariable('Abrir la Imagen', 'Abrir en nueva ventana');
			//fo.addVariable('langAbout', 'About');	
			fo.addVariable('xmlURL', '$galeriaxml');					
		
			fo.write('flashcontent');
		</script>	";
}


// muestra en pantalla sobre una propiedad en particular
function muestraPropiedadEdt($conexion, $idPropiedad, $nombreTab, $action, $nameForm, $otrosDatos)
{
	if($idPropiedad == 0)
	{
		return "<div align='center'><h2>Propiedad no encontrada</h2></div>";
		// mejor mandar un grupo de banners
	}
	else
	{
		$propiedad = consultaPropiedad($conexion, $idPropiedad); 
		return             
			generaformulario(array(
					array("<b>Nombre:</b>",	genImput("nombre", 20, "$propiedad[nombre]", "edt", 50)),
					array("<b>Descripcion:</b>", genImput("descripcion", 20, "$propiedad[descripcion]", "edt", 50)),
					array("<b>Direccion:</b>", genImput("direccion", 20, "$propiedad[direccion]", "edt", 50)),
					array("<b>Requisitos:</b>", genImput("requisitos", 20, "$propiedad[requisitos]", "edt", 50)),
					array("<b>Provincia:</b>",selectProvincias($conexion,  "", "", "edt")),
					array("<b>Localidad:</b>", genImput("localidad", 20, "$propiedad[localidad]", "edt", 50)),
					array("<b>Documentacion:</b>", genImput("documentacion", 20, "$propiedad[documentacion]", "edt", 50)),
					array("<b>Costo:</b>",genImput("costo", 20, "$propiedad[costo]", "edt", 50)),
					array("<b>Tipo:	</b>", selectTipoProp($conexion,  "", "", "edt")),
					array("<b>Luz</b>",genImput("luz", 10, "$propiedad[luz]", "edt", 30)),
					array("<b>Agua</b>", genImput("agua", 10, "$propiedad[agua]", "edt", 30)),
					array("<b>Gas</b>", genImput("gaz", 10, "$propiedad[gaz]", "edt", 30)),
					array("<b>Cloacas</b>",genImput("cloacas", 10, "$propiedad[cloacas]", "edt", 30)),
					array("<b>Asfalto</b>",genImput("asfalto", 10, "$propiedad[asfalto]", "edt", 30)),
					array("<b>Otros Datos</b>",generaText("otros", 10, 20, "$propiedad[otros]", "edt"))
					), 'tabla', $action, $nameForm, $otrosDatos);
	}
}

function muestraPropiedad($conexion, $idPropiedad, $nombreTab)
{
	if($idPropiedad == 0)
	{
		return "<div align='center'><h2>Propiedad no encontrada</h2></div>";
		// mejor mandar un grupo de banners
	}
	else
	{
        $propiedad = consultaPropiedad($conexion, $idPropiedad); 
        $galeria = "";
        if($propiedad["script"] != "")
        {
            $galeria = galeriaFotos($propiedad["script"]);
        }
		
		return 	
        
            $galeria.
			genTabla(array(
					array("<b>Nombre:</b>",	"$propiedad[nombre]"),
					array("<b>Descripcion:</b>", "$propiedad[descripcion]"),
					array("<b>Direccion:</b>", "$propiedad[direccion]"),
					array("<b>Requisitos:</b>", "$propiedad[requisitos]"),
					array("<b>Provincia:</b>", "$propiedad[provincia]"),
					array("<b>Localidad:</b>", "$propiedad[localidad]"),
					array("<b>Documentacion:</b>", "$propiedad[documentacion]"),
					array("<b>Costo:</b>", "$propiedad[costo]"),
					array("<b>Tipo:	</b>", "$propiedad[tipo]"),
					array("<b>Luz</b>", "$propiedad[luz]"),
					array("<b>Agua</b>", "$propiedad[agua]"),
					array("<b>Gas</b>", "$propiedad[gaz]"),
					array("<b>Cloacas</b>", "$propiedad[cloacas]"),
					array("<b>Asfalto</b>", "$propiedad[asfalto]")
					
					), 'tabla',"");
	}
}

// muestra un banner horizontal
function bannerHorizontal($conexion, $idPropiedad)
{
	$propiedad = consultaPropiedad($conexion, $idPropiedad);
    $codigo ="";
    if($idPropiedad !=0)
    {
        $adicionales = "";
        if($propiedad["mapa"] != "")
        {
            $adicionales .= genLink("Ver Mapa", $propiedad["mapa"],"")." - ";
        }        
        
        $codigo =		genTabla(array(
							array(
								genLink(muestraImagen($propiedad["imgBanner"], "imgBanner", ""),"servicios.php?prop=".$propiedad["idProp"],""),
								genLink("<h3>$propiedad[nombre]</h3>","servicios.php?prop=".$propiedad["idProp"],"").
								$propiedad['descripcion']." Provincia: $propiedad[provincia]. Localidad:$propiedad[localidad]. ".                               
                                $adicionales.                              
								genLink("Ver Detalles", "servicios.php?prop=".$propiedad["idProp"],"")." - ".
								genLink("Contacto", "contacto.php?id=".$propiedad["idProp"],"")
							)), 'tabla', '');
    }
	return $codigo;
}

//genera un mensaje de texto segun alguna consulta
function mensajePropiedad($conexion, $idPropiedad)
{
	if($idPropiedad == 0)
	{
		return "";
	}
	else
	{
		$propiedad = consultaPropiedad($conexion, $idPropiedad);
		return "Consulta sobre propiedad nro ".$propiedad["idProp"].
				". Quisiera saber sobre la propiedad ".$propiedad["nombre"].
				" Quisiera saber todos los datos correspondientes y que se comuniquen conmigo en los medios que estoy registrando";
	}
}

//muestra un banner vertical
function bannerVertical($conexion, $idPropiedad)
{
	$propiedad = consultaPropiedad($conexion, $idPropiedad);
    $imagen = "<div id='bannerPrincipal'>".
                    genLink(muestraImagen($propiedad["imgBanner"], "imgBanner", "").
                    "<p>$propiedad[nombre]. $propiedad[localidad]; </p>","servicios.php?prop=".$idPropiedad,"").
                 "</div>";
    return  $imagen;
}

// realiza la consulta de una propiedad
function consultaPropiedad($conexion, $idPropiedad)
{
	return consultaSQLbasicaRow($conexion, "vistapropiedad", "*", "idProp=$idPropiedad");
}

//modifica los datos de una propiedad
function modificaPropiedad($conexion, $campos, $valores, $condicion)
{
	return modificarSQL($conexion, "propiedad", $campos, $valores, $condicion);
}
/* Tablas en seleccion*/

//Muestra las provincias en un select
function selectProvincias($conexion,  $condicion, $predeterminado, $estilo)
{
	return selectTablaSQL($conexion, "provincia", "idProvincia", "nombreProv", $condicion, $predeterminado, $estilo);
}

// muestra los tipos de propiedades
function selectTipoProp($conexion,  $condicion, $predeterminado, $estilo)
{
	return selectTablaSQL($conexion, "tipoprop", "idTipoPro", "tipoProp", $condicion, $predeterminado, $estilo);
}

//genera un grupo de banner para su promocion
function generaBanner($conexion, $cantidad, $condicion)
{
    if($condicion == "")
    {
        $cant = consultaSQLbasicaRow($conexion, "propiedad", "COUNT('*') AS cantidad", "estado = 1");
        $lista = consulSQLbasica($conexion, "vistapropiedad", "*", "estado = 1 ORDER BY RAND()");
    }
    else
    {
        $cant = consultaSQLbasicaRow($conexion, "propiedad", "COUNT('*') AS cantidad", "estado = 1 AND ".$condicion);
        $lista = consulSQLbasica($conexion, "vistapropiedad", "*", "estado = 1 AND ".$condicion." ORDER BY RAND()");
    }    
    
    $count = "";
    if($cant["cantidad"] < $cantidad)
    {
    
        $cantidad = $cant["cantidad"];        
        while ($cantidad > 0)
        {    
            $propiedad = mysql_fetch_array($lista);        
            $count .= bannerHorizontal($conexion, $propiedad["idProp"]);
            
            $cantidad --;
        }
    }
    else
    {
        while($cantidad > 0)
        {            
            $count .= bannerHorizontal($conexion, $propiedad["idProp"]);
            $propiedad = mysql_fetch_array($lista);
            $cantidad --;
        }
    }    
    return $count;
}

// genera los banner de manera aleatorios
function generaBannerHorizontal($conexion, $cantidad, $condicion)
{
    if($condicion == "")
    {
        $cant = consultaSQLbasicaRow($conexion, "propiedad", "COUNT('*') AS cantidad", "estado = 1");
        $lista = consulSQLbasica($conexion, "vistapropiedad", "*", "estado = 1 ORDER BY RAND() ");
    }
    else
    {
        $cant = consultaSQLbasicaRow($conexion, "propiedad", "COUNT('*') AS cantidad", "estado = 1 AND ".$condicion);
        $lista = consulSQLbasica($conexion, "vistapropiedad", "*", "estado = 1 AND ".$condicion." ORDER BY RAND() ");
    }    
    
    $propiedad = mysql_fetch_array($lista);
    $count = "";
    if($cant["cantidad"] < $cantidad)
    {
        $cantidad = $cant["cantidad"];
    }
    while($cantidad > 0)
    {            
        $count .= bannerVertical($conexion, $propiedad["idProp"]);
        $propiedad = mysql_fetch_array($lista);
        $cantidad --;
    }    
    return $count;
}

/* Funciones Adicionales*/
// genera vectores aleatorios
function vector_aleatorio($array)
{
    $cant= count($array);
    for($i=0;$i<$cant-1;$i++)
    {
        $j = rand(0,$cant-1);
        $aux = $array[$i];
        $array[$i] = $array[$j];
        $array[$j] = $array[$i];
    }    
    return $array;
}

?>
<?php
/*
Libreria de publicacion de redes sociales
# Sitio web de Uriel Propiedades
#Autor : Ariel Marcelo Diaz
#Sitio Web: http://www.infrasoft.com.ar
# Licencia : GPL2 o Superiores  
*/

class facebook{

    public $opcion=1;
    public $sitio = "http://urielpropiedades.com.ar/";
    public $link="";
    public $titulo = "Uriel Propiedades";
    public $url_imagen = "";
    public $post = 7;

    // inserta los datos en cabecera del sitio
    function cabecera()
    {
        return "<meta property='og:site_name' content='".$this->sitio."' />
                <meta property='og:title' content='".$this->titulo."' />
                <meta property='fb_title' content='".$this->titulo."' />
                <meta property='og:image' content='".$this->url_imagen."' />".
                "<div id='fb-root'></div>
                <script>
                    (
                        function(d, s, id)
                        {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = '//connect.facebook.net/es_LA/all.js#xfbml=1';
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                        (document, 'script', 'facebook-jssdk'));</script>";
    }
    
    //muestra el link completo del sitio
    function web()
    {
        return $this->sitio.$this->link;
    }
    
    // inserta un like en el sitio
    function like()
    {
            return "<div class='fb-like' 
                            data-href='".$this->sitio."'
                            data-layout='button' 
                            data-action='like' 
                            data-show-faces='true'
                            data-share='true'></div>";
    }
    
    //funcion comentario
    function comentario()
    {
        return "<div class='fb-comments' 
                            data-href='".$this->web()."'
                            data-numposts='".$this->post."' 
                            data-colorscheme='light'
                            ></div>";
    }
    
    // funcion de recomendacion
    function recomendacion()
    {
        $var="<div class='fb-recommendations-bar' 
                data-site='".$this->web()."' 
                data-read-time='30' 
                data-side='right' 
                data-action='recommend'></div>";
        if($this->opcion==1)
        {
            return $var;
        }
        else
        {
            echo $var;
        }
    }
    
    //funcion registracion en facebook
    function registracion()
    {
    
    }
}
?>
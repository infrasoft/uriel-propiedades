SELECT 
	idProp, nombre, descripcion, requisitos,
	localidad, documentacion, costo,
	plano, estado, luz, direccion,
	gaz, cloacas, agua, asfalto,
	nombreProv as provincia,
	tipoProp, imgBanner, script, mapa,
	tipooperacion.tipo
FROM
	propiedad, provincia, tipooperacion, tipoprop
WHERE 
	provincia.idProvincia = propiedad.provincia
AND
	tipooperacion.idTipo = propiedad.operacion
AND
	tipoprop.idTipoPro = propiedad.tipo;
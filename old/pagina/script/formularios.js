// JavaScript Document
/*************************************
 *****Club Deportivo Gran Bourg********
 ********Sitio Web****************
 
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
*Sitio Web: http://www.infrasoft.com.ar 
****************************************/ 

// actualiza el valor del captcha
function actucap()
{   
    var obj = document.getElementById("captcha");
        num = 0;
    if (!obj)
    {
        obj = window.document.all.captcha;
    }  
    num ++;   
    if (obj)
    {
      obj.src = "captcha.php?" + Math.random() + num;    
    }  
}

// valida un email
function valEmail(valor)
{
    re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
    if(!re.exec(valor))   
     {
        return false;
    }
    else
    {
        return true;
    }
}

// verifica los datos del formulario de contacto

function validarContacto(contacto)
{
  if(contacto.nombre.value.length <=4) 
  {
     alert ("El nombre debe tener al menos 4 caracteres");
     contacto.nombre.focus();
     return false;
  }
  else
  {
    if(contacto.celular.value.length <=4)
    {
        alert("No es un celular valido");
        contacto.celular.focus();
        return false;
    }
    else
    {
        if((contacto.mail.value.length <=7) )  
        {
            alert ("Email no valido");
            contacto.mail.focus();
            return false;
        }
        else
        {
            if(contacto.asunto.value.length==0)
            {
                alert ("El asunto no debe estar vacio");
                contacto.asunto.focus();
                return false;
            }
            else
            {
                if(contacto.comentarios.value.length==0)
                {
                    alert ("Los comentarios no deben estar vacios");
                    contacto.comentarios.focus();
                    return false;
                }
                else
                {
                    if(contacto.tmptxt.value.length==0)
                    {
                        alert ("La verificacion no debe ser nula");
                        contacto.tmptxt.focus();
                        return false;
                    }
                    else
                    {
                        alert ("Gracias por completar el formulario");
                        contacto.submit();
                        return true;
                    }
                }
            }
        }
    }
     
  }
}

// valida el formulario de nuevo usuario
/*
function altaUsuario(nuevoUsu)
{
  if(nuevoUsu.apellido.value.length))
  { 
    alert ("El apellido no debe estar vacio");
    nuevoUsu.apellido.focus();
    return false;  
  }
  else
  {
    if(nuevoUsu.nombre.value.length)
    {
      alert ("El nombre no debe estar vacio");
      nuevoUsu.nombre.focus();
      return false;
    }
    else
    {
      alert ("Gracias por completar el formulario");    
    }  
  }
}
 */
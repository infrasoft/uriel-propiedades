<?php
/**********************************************
 ***** Controlador de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 /**
  *  Controlador para el manejo de la administracion de propiedades
  */
 class Administracion extends CI_Controller 
 {
     
     function __construct() 
     {
         parent::__construct();
		 $this->load->library('upload');
		 $this->load->library('image_lib');
     }
	 
	 public function index()//muestra la lista de propiedades
	 {
	 	if ($this->session->userdata("act") == TRUE)
		{
			$this->load->view("head");
         	$this->load->view("backend/head-main-adm.php");
			$buscar = $this->input->post("buscar");
			
			if (!isset($buscar))
			{
				$data['datos'] = $this->Propiedades_mdl->consulta("");
			}
			 else
		    {
				$data["datos"] = $this->Propiedades_mdl->consulta(
									"nombre LIKE '%".$buscar."%' OR ".
									"descripcion LIKE '%".$buscar."%' OR ".
									"provincia LIKE '%".$buscar."%' OR ".
									"localidad LIKE '%".$buscar."%' OR ".
									"tipo LIKE '%".$buscar."%' OR ".
									"requisitos LIKE '%".$buscar."%' OR ".
									"operacion LIKE '%".$buscar."%' OR ".
									"documentacion LIKE '%".$buscar."%' OR ".
									"direccion LIKE '%".$buscar."%' OR ".
									"otros LIKE '%".$buscar."%' ORDER BY idProp desc"); 
									
			}
			
		 	
         	$this->load->view("backend/lista-prop", $data);
         	$this->load->view("footer");
		}
		else
	    {
			 redirect("/seguridad/",'refresh');
	    }	 
		
	 }
	 
	 //carga los datos del formulario
	 private function formulario()
	 {
		 $data['nombre'] = $this->input->post('nombre');
		 $data['descripcion'] = $this->input->post('descripcion');
		 $data['provincia'] = $this->input->post('provincia');
		 $data['localidad'] = $this->input->post('localidad');
		 $data['direccion'] = $this->input->post('direccion');
		 $data['tipo'] = $this->input->post('tipo');
		 $data['requisitos'] = $this->input->post('requisitos');
		 $data['operacion'] = $this->input->post('operacion');		 
		 $data['documentacion'] = $this->input->post('documentacion');
		 $data['precio'] = $this->input->post('precio');
		 $data['planos'] = $this->input->post('planos');
		 $data['agua'] = $this->input->post('agua');
		 if ($data['agua'] != "si")
		 {
			$data['agua']= "no"; 
		 }		 
		 $data['luz'] = $this->input->post('luz');
		 if ($data['luz'] != "si")
		 {
			$data['luz']= "no"; 
		 }
		 $data['gaz'] = $this->input->post('gaz');
		 if ($data['gaz'] != "si")
		 {
			$data['gaz']= "no"; 
		 }
		 $data['cloacas'] = $this->input->post('cloacas');
		 if ($data['cloacas'] != "si")
		 {
			$data['cloacas']= "no"; 
		 }
		 $data['asfalto'] = $this->input->post('asfalto');
		 if ($data['asfalto'] != "si")
		 {
			$data['asfalto']= "no"; 
		 }
		 
		 $data['sup_construida'] = $this->input->post('sup_construida');
		 $data['sup_semidescubierta'] = $this->input->post('sup_semidescubierta');
		 $data['sup_descubierta'] = $this->input->post('sup_descubierta');
		 $data['sup_total'] = $this->input->post('sup_total');
		 
		 
		 $data['mapa'] = $this->input->post('mapa');		 
		 $data['estado'] = $this->input->post('estado');
		 $data['otros'] = $this->input->post('otros');
		 return $data;
	 }
	 
	 //realiza la subida de archivos
	 private function subir($carpeta="",$archivo="") //falta reducir las imagenes
	 {
	 	if ($this->session->userdata("act") == TRUE)
		{
		  if (!empty($_FILES[$archivo]['name']))
          {
         	  $dire = "media/img/".$carpeta."/";
         	  if (!is_dir($dire))
			  {
				 mkdir($dire, 0700);
			  }		
			  //aqui implementar lo de la reduccion de imagenes		  	
		  	  $config['upload_path'] = $dire;
          	  $config['file_name'] = $carpeta.$_FILES[$archivo]['name'] ;
          	  $config['allowed_types'] = "gif|jpg|jpeg|png";
           	  $config['max_size'] = "50000";
          	  $config['max_width'] = "2000";
          	  $config['max_height'] = "2000";
			  $config['width'] = "800";
          	  $config['height'] = "600";
       	  	  
			  /*$this->image_lib->initialize($config);
			  $this->image_lib->resize();
			  if (!$this->image_lib->resize())
			  {
            	echo $this->image_lib->display_errors('', '');
        	  }*/
			  
        	  $this->upload->initialize($config);
          	  if (!$this->upload->do_upload($archivo))
		   	  {//*** ocurrio un error
        		 $data['uploadError'] = $this->upload->display_errors();
        	     echo $archivo.":".$this->upload->display_errors();
		      }					
			  $data['uploadSuccess'] = $this->upload->data();
		  }
		}
		else
	    {
			 redirect("/seguridad/",'refresh');
	    }
		
	 }
	 
	 //genera una nueva propiedad
	 public function nueva_prop()
	 {
	 	if ($this->session->userdata("act") == TRUE)
		{
			$this->load->view("head");
         	$this->load->view("backend/head-main-adm.php");
		 	//datos del formulario
		 	$data = $this->formulario();		 
		 	if (($data['nombre'] != null)  &&
				($data['direccion'] != null) &&
				($data['precio'] != null))
		 	{
		 	 	$data["banner"] = $_FILES['banner']['name'];
			 	$data["imagenes1"] = $_FILES['imagenes1']['name'];
			 	$data["imagenes2"] = $_FILES['imagenes2']['name'];
			 	$data["imagenes3"] = $_FILES['imagenes3']['name'];
			 	$data["imagenes4"] = $_FILES['imagenes4']['name'];
			 	$data["imagenes5"] = $_FILES['imagenes5']['name'];		 					 
			 	if ($this->Propiedades_mdl->nueva($data))// falta para multiples datos
			 	{// ver la redimencion de imagenes
					$id_prop = $this->db->insert_id();
				 	// print_r($data);				 
				 	$this->subir("portada","banner");
				 	$this->subir($id_prop,"imagenes1");
				 	$this->subir($id_prop,"imagenes2");
				 	$this->subir($id_prop,"imagenes3");
				 	$this->subir($id_prop,"imagenes4");
				 	$this->subir($id_prop,"imagenes5");
				   
				  	$data2["mensaje"] = "<p class='text-primary'>Datos Cargados correctamente</p>";
				 	$last = $this->db->insert_id();
				 
			 	}
			  	else
			 	{
					 $data2["mensaje"] = "<p class='text-danger'>Datos no cargados</p>";
			 	}					 
		 	}
		 	else 
			{
				$data2["mensaje"] = ""; 
		    }	 		
				 
		 	$data2["titulo"]="Nueva Propiedad";
         	$this->load->view("backend/nueva-prop",$data2);
         	$this->load->view("footer");
		}
		else
	    {
			 redirect("/seguridad/",'refresh');
	    }		 
	 }
	 
	 //modifica las propiedades
	 public function modifica_prop($id=0)// cuidado para la actualizacion de imagenes
	 {
	 	if ($this->session->userdata("act") == TRUE)
		{
			$this->load->view("head");
         	$this->load->view("backend/head-main-adm.php");
		  	//datos del formulario
		 	$data = $this->formulario();
		 	$data2["mensaje"] = "";
         	if (($id != 0) && ($data['nombre'] != null)  &&
			($data['direccion'] != null) &&
			($data['precio'] != null))
         	{
         	 	$data["banner"]=$_FILES['banner']['name'];
			 	$data["imagenes1"]=$_FILES['imagenes1']['name'];
			 	$data["imagenes2"]=$_FILES['imagenes2']['name'];
			 	$data["imagenes3"]=$_FILES['imagenes3']['name'];
			 	$data["imagenes4"]=$_FILES['imagenes4']['name'];
			 	$data["imagenes5"]=$_FILES['imagenes5']['name'];
			    if ($this->Propiedades_mdl->modifica($id,$data)) //realizar subida de imagenes
                { 	
				 	$this->subir("portada","banner");
				 	$this->subir($id,"imagenes1");
				 	$this->subir($id,"imagenes2");
				 	$this->subir($id,"imagenes3");
				 	$this->subir($id,"imagenes4");
				 	$this->subir($id,"imagenes5");
             	
                 	$data2["mensaje"] = "<p class='text-primary'>Datos Modificados correctamente</p>";
             	}
			  	else
			 	{
                    $data2["mensaje"] = "<p class='text-danger'>Datos no modificados</p>";
             	}             
         	}         
         
         	$data2["titulo"]="Modificar datos de propiedad";
         	$data2["id"] = $id;
         	$this->load->view("backend/modifica-prop",$data2);
         	$this->load->view("footer");
		}
		else
	    {
			redirect("/seguridad/",'refresh'); 
	    }
		
		 
	 }
	 
	 //muesta la lista de comentarios del sitio
	 public function listacomentarios()
	 {	 			
	 	if ($this->session->userdata("act") == TRUE)
		{
			$this->load->view("head");
         	$this->load->view("backend/head-main-adm");
		 
		 	$this->load->model("Comentarios_mdl");		 
		 	$data['datos'] = $this->Comentarios_mdl->consulta("1 ORDER BY idProp desc");
		 
		 	$this->load->view("backend/lista-comentarios",$data); 
		 	$this->load->view("footer"); 
		}
		else
	    {
			 redirect("/seguridad/",'refresh');
	    }
	 	
		
	 }
	 
	 //realiza la modificacion de los comentarios del sitio
	 public function modifica_comentario($id=0)
	 {
	 	
		if ($this->session->userdata("act") == TRUE)
		{
			$this->load->view("head");
         	$this->load->view("backend/head-main-adm.php");
		 
		 	$this->load->view("footer");
		}
		else
	    {
			 redirect("/seguridad/",'refresh');
	    }
	    
		 
	 }
	 
	 //muestra la lista de mensajes del sitio
	 public function lista_mensajes()
	 {
	 	if ($this->session->userdata("act") == TRUE)
		{
			$this->load->view("head");
         	$this->load->view("backend/head-main-adm");
		 
		 	$this->load->model("Mensajes_mdl");
		 	$data['datos'] = $this->Mensajes_mdl->consulta("1 ORDER BY id_mensaje desc");
		 
		 	$this->load->view("backend/lista-mensajes",$data);
		 	$this->load->view("footer");
		}
		else
	    {
			 redirect("/seguridad/",'refresh');
	    }		 
	 } 
	 
 }
 
 ?>
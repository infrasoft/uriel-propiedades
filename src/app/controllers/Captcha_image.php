<?php
/**********************************************
 ***** Controlador de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 
 /**
  * Para poder generar el captcha
  */
 class Captcha_image extends CI_Controller
 {     
     function __construct() 
     {
     	parent::__construct();    
     }
	 
	 public function index()
	 {
		 #crea la imagen y el fondo de color
		$captcha = imagecreatetruecolor(120,35);
		$color = rand(150,250);
		$background_color = imagecolorallocate($captcha, $color, $color, $color);
		imagefill($captcha, 0, 0, $background_color);
		
		#draw some lines
		for($i=0;$i<10;$i++)
		{
			$color = rand(48,96);
			imageline($captcha, rand(0,130),rand(0,35), rand(0,130), rand(0,35),imagecolorallocate($captcha, rand(150,250), rand(150,250), rand(150,250)));
		}
		
		$this->load->library('Random');
		$string = $this->random->genera();
		$_SESSION["captchatxt"] = $string;
		#place each character in a random position
		$font = 'fuentes/arial.ttf';
		for($i=0;$i<5;$i++)
    	{
			$color = rand(0,32);
			if(file_exists($font))
        	{
				$x=4+$i*23+rand(0,6);
				$y=rand(18,28);
				imagettftext  ($captcha, 15, rand(-25,25), $x, $y, imagecolorallocate($captcha, $color, $color, $color), $font, $_SESSION["captchatxt"][$i]);
			}
        	else
        	{
				$x=5+$i*24+rand(0,6);
				$y=rand(1,18);
				imagestring($captcha, 5, $x, $y, $_SESSION["captchatxt"] [$i], imagecolorallocate($captcha, $color, $color, $color));
			}
		}
	
		#applies distorsion to image
		$matrix = array(array(1, 1, 1), array(1.0, 7, 1.0), array(1, 1, 1));
		imageconvolution($captcha, $matrix, 16, 32);
		
		
		#avoids catching
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); 

		#return the image
		header("Content-type: image/gif");
		imagejpeg($captcha);
	}
 }
 
 ?>
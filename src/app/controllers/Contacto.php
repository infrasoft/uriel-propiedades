<?php
/**********************************************
 ***** Controlador de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
defined('BASEPATH') OR exit('Acceso no permitido');

/**
 *  Clase para el manejo de contacto
 */
class Contacto extends CI_Controller 
{
	
	function __construct()
    {
		parent::__construct();
		
		$this->load->library('email');		
		/*$this->load->library('botdetect/BotDetectCaptcha', array(
    	'captchaConfig' => 'ExampleCaptcha' ));*/
		$this->load->helper('captcha');
		
	}
	
		
	//envio de email
	private function enviar($email="",$asunto="", $contenido="")
	{
		//envio de mail
		$this->email->from('info@urielpropiedades.com.ar', 'Ariel Marcelo Diaz');
		$this->email->to($email);
		$this->email->cc("marcelodiaz96@gmail.com");
		$this->email->bcc('sraulcantero@gmail.com');
		$this->email->subject($asunto);
		$this->email->message($contenido);
		return $this->email->send();
	}
	
	public function index()// sin terminar
	{
		 $this->load->view("head");
		 $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si'");
         $this->load->view("head-main",$data2);
		 // make Captcha Html accessible to View code
  		 //$data['captchaHtml'] = $this->botdetectcaptcha->Html();  		 
		 $data["mensaje"] = "";
		 $email = $this->input->post("email");
		 $asunto = $this->input->post("asunto");		 	
		 $contenido = "Datos del correo:\n".
		                    "Apellido:".$this->input->post('apellido')."\n".
							"Nombre:".$this->input->post('nombre')."\n".
							"Email:".$this->input->post('email')."\n".
							"Telefono:".$this->input->post('telefono')."\n".
							"Direccion:".$this->input->post('direccion')."\n".
							"Mensaje:".$this->input->post('mensaje')."\n" ;
		 $data["mensaje"] = "";	
		 $code=$this->input->post("CaptchaCode");
		 
		 if (($email != null)&&($asunto!= null)&&($contenido!= null) ) 
		 {	//tratamos los campos del formulario y comprobamos
		 	$this->form_validation->set_rules('apellido', 'apellido', 'required|trim|min_length[3]|max_length[40]');
			$this->form_validation->set_rules('nombre', 'nombre', 'required|trim|min_length[4]|max_length[40]');
			$this->form_validation->set_rules('email', 'email', 'required|trim|min_length[3]|max_length[40]');	
			$this->form_validation->set_rules('telefono', 'telefono', 'required|trim|min_length[3]|max_length[40]');
			$this->form_validation->set_rules('mensaje', 'mensaje', 'required|trim|min_length[3]|max_length[40]');
			
			
						
			//lanzamos mensajes de error si es que los hay
			$this->form_validation->set_message('required', 'El %s es requerido');
		    $this->form_validation->set_message('min_length', 'El %s debe tener al menos %s car&aacute;cteres');
			$this->form_validation->set_message('max_length', 'El %s debe tener al menos %s car&aacute;cteres');
			
			if ($this->form_validation->run())// verificar 
			{
				if ($code==$_SESSION["captchatxt"]) 
				{
					$this->enviar($email,$asunto,$contenido);
					$data["mensaje"] = "Mensaje enviado correctamente";				
				} 
				else
		   		{
					$data["mensaje"] = "Error en el envio de email. 
					Por favor intente enviarlo nuevamente";
				}
			}
		 	else 
		 	{
			 	$data["mensaje"] = validation_errors();	
		 	}
		 }
		 $this->load->view("frontend/contacto",$data);
		 $this->load->view("footer"); 
	}			
			
	public function consulta_propiedad($idProp=0)//aun sin probar	
	 {
		 $this->load->view("head");
		 $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si'");
         $this->load->view("head-main",$data2);
         
		 $email = $this->input->post("email");
		 $asunto = $this->input->post("asunto");		 	
		 $contenido = "Datos del correo:\n".
		                    "Apellido:".$this->input->post('apellido')."\n".
							"Nombre:".$this->input->post('nombre')."\n".
							"Email:".$this->input->post('email')."\n".
							"Telefono:".$this->input->post('telefono')."\n".
							"Direccion:".$this->input->post('direccion')."\n".
							"Propiedad:".$this->input->post('idProp')."\n".
							"Mensaje:".$this->input->post('mensaje')."\n" ;
		 $code=$this->input->post("CaptchaCode");
		 $data["mensaje"] = "";	
		 	
		 if (($email != null)&&($asunto!= null)&&($contenido!= null)) 
		 {
		 	if ($code==$_SESSION["captchatxt"]) 
		 	{
				if ($this->enviar()) 
				{
					$data["mensaje"] = "Mensaje enviado correctamente";				
				} 
				else
		    	{
					$data["mensaje"] = "Error en el envio de email. 
						Por favor intente enviarlo nuevamente";
				} 
			}
			else
			{
				$data["mensaje"] = "Error en el codigo de verificacion"; 
			}			 
			
		 }
		 
		 
         if ($idProp==0)
		 {//redireccionar a contactos
             redirect("contacto/","refresh");
         }
		  else
		 {
             $data["idProp"] = $idProp; 
			 $consulta=$this->Propiedades_mdl->consulta("idProp=".$idProp);
			 foreach ($consulta as $row) 
  	  		 {
  	  		 	$data["nombre"] = $row->nombre."<br/>".$row->descripcion;
  	  		 }
			 
             $this->load->view("frontend/contacto-consulta-propiedad", $data);
         }         
		 
		 $this->load->view("footer");
	 }
	 
	 //sin implementar
	 public function recepcion_mensajes()
	 {
		 $this->load->view("head");
		 $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si'");
         $this->load->view("head-main",$data2);
		 $data["titulo"] = "Recepcion de mensajes";
		 // make Captcha Html accessible to View code
  		 $data['captchaHtml'] = $this->botdetectcaptcha->Html();  		 
		 $data["mensaje"] = "";
		 $email = $this->input->post("email");
		 $asunto = $this->input->post("asunto");		 	
		 $contenido = "Datos del correo:\n".
		                    "Apellido:".$this->input->post('apellido')."\n".
							"Nombre:".$this->input->post('nombre')."\n".
							"Email:".$this->input->post('email')."\n".
							"Telefono:".$this->input->post('telefono')."\n".
							"Tipo de Inmueble:".$this->input->post('tipo-inmueble')."\n".
							"Antiguedad:".$this->input->post('antiguedad')."\n".
							"Mt2 descubiertos:".$this->input->post('mt2_descubiertos')."\n".
							"Mt2 semi cubiertos:".$this->input->post('mt2_semicubiertos')."\n".
							"Mt2 Totales:".$this->input->post('mt2')."\n".
							"Cantidad de ambientes:".$this->input->post('ambientes')."\n".
							"Estado de conservacion:".$this->input->post('conservacion')."\n".
							"Calidad de Construccion:".$this->input->post('calidad-const')."\n".
							"Cantidad de cocheras:".$this->input->post('cocheras')."\n".
							"Estado de conservacion:".$this->input->post('conservacion')."\n";
		 $data["mensaje"] = "";				
		 if (($email != null)&&($asunto!= null)&&($contenido!= null) ) 
		 {
			if ($this->enviar()) 
			{
				$data["mensaje"] = "Mensaje enviado correctamente";				
			} 
			else
		    {
				$data["mensaje"] = "Error en el envio de email. Por favor intente enviarlo nuevamente";
			}
		 }
		 
		 $data["captcha"] = $this->captcha();
		 $data = $this->security->xss_clean($data);
		 $this->load->view("frontend/mensajes", $data);
		 $this->load->view("footer");
	 }

	//probando un par de cosas
	public function test()
	{
		$this->load->model('Captcha_mdl');
		
		$this->load->library('form_validation');
 
    // reglas de validacion
    $this->form_validation->set_rules('captcha','Captcha','required|callback_captcha_check');
    $this->form_validation->set_message('captcha_check','El código ingresado no es válido');
 
    if ($this->form_validation->run() == FALSE) {
 
        // Creacion del Captcha
        $this->load->helper('captcha');
 
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url().'media/img/captcha.php',
            'font_path' => './sys/fonts/texb.ttf',
            'img_width' => '150',
            'img_height' => 30,
            'expiration' => 7200
        );
 
        $cap = create_captcha($vals);
 
 if (isset($cap))
	{
		echo "Es Nulo";	
	}
	else
    {
		if (is_array($cap))
		{
			echo "Es un array";
		} 
		else
	    {
			echo "No es un array";
		}
		
	}
	
        // se agrega el captcha a la base de datos
        $captcha_info = array (
            'captcha_time' => $cap['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => $cap['word']
        );
 
        $this->Captcha_mdl->insertCaptcha($captcha_info);
 
        $data['cap'] = $cap;
        //$this->load->view('formulario',$data);
        echo "formulario";
    	}
		 else
	   {
          echo "funciono";
    }
	
	
	
	}
}
	

?>
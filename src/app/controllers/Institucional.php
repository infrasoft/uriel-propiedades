<?php
/**********************************************
 ***** Controlador de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 /**
  *  Clase para el manejo de datos institucionales
  */
 class Institucional extends CI_Controller
 {
     
     function __construct()
     {
         parent::__construct();
     }
	 
	 public function index()
	 {
		 $this->load->view("head");
		 $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si'");
         $this->load->view("head-main",$data2);
         $this->load->view("frontend/institucional");
         $this->load->view("footer");
	 }
 }
 
 ?>
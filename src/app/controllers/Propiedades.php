<?php
/**********************************************
 ***** Controlador de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 /**
  *  Controlador para el manejo de propiedades
  */
 class Propiedades extends CI_Controller 
 {
     
     function __construct()
     {
         parent::__construct();		 
		 $this->load->helper("galeria_helper");
     }
     
     public function index($id=0) //muestra las propiedades
     {
         $this->load->view("head");
		 $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si' AND estado='abierta'");
         $this->load->view("head-main",$data2); 
		 
		 $buscar = $this->input->post("dato");
		 if (isset($buscar)) 
		 {
			 $data["consulta"] = $this->Propiedades_mdl->consulta(
		 							"estado='abierta' ORDER BY idProp desc LIMIT ".$id.",9");
		 	 $data["cantidad"] = $this->Propiedades_mdl->nro_consulta("estado='abierta'");
		 } 
		 else 
		 {
			$data["consulta"] = $this->Propiedades_mdl->consulta(
									"(nombre LIKE '%".$buscar."%' OR ".
									"descripcion LIKE '%".$buscar."%' OR ".
									"provincia LIKE '%".$buscar."%' OR ".
									"localidad LIKE '%".$buscar."%' OR ".
									"tipo LIKE '%".$buscar."%' OR ".
									"requisitos LIKE '%".$buscar."%' OR ".
									"operacion LIKE '%".$buscar."%' OR ".
									"documentacion LIKE '%".$buscar."%' OR ".
									"direccion LIKE '%".$buscar."%' OR ".
									"otros LIKE '%".$buscar."%' )".
		 							" AND estado='abierta' ORDER BY idProp desc LIMIT ".$id.",9"); 
									
			$data["cantidad"] = $this->Propiedades_mdl->nro_consulta(
									"nombre LIKE '%".$buscar."%' OR ".
									"descripcion LIKE '%".$buscar."%' OR ".
									"provincia LIKE '%".$buscar."%' OR ".
									"localidad LIKE '%".$buscar."%' OR ".
									"tipo LIKE '%".$buscar."%' OR ".
									"requisitos LIKE '%".$buscar."%' OR ".
									"operacion LIKE '%".$buscar."%' OR ".
									"documentacion LIKE '%".$buscar."%' OR ".
									"direccion LIKE '%".$buscar."%' OR ".
									"otros LIKE '%".$buscar."%' ".
									"AND estado='abierta'");
		 }
		 
		 
		 
		 
         $this->load->view("frontend/propiedades",$data);
         
         $this->load->view("footer");
     }
	 
	 public function detalle($id=0)
	 {
		 $this->load->view("head");         
        
		 $data["consulta"] = $this->Propiedades_mdl->consulta("idProp=".$id); 
		 $this->load->view("frontend/head-detalle-prop",$data);
		 $this->load->view("frontend/detalle-propiedades",$data);
		 $this->load->view("footer");
	 }
 }
 
 
 ?>
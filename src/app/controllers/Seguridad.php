<?php
/**********************************************
 ***** Controlador de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 /**
  *  Controlador para el manejo de Servicios
  */
 class Seguridad extends CI_Controller 
 {
     
     function __construct() 
     {
         parent::__construct();
				 
		 $this->load->model('Us_mdl');
     }
	 
	 public function index()
	 {
		$this->load->view("head");  
		$data["mensaje"] = $this->session->userdata("mensaje");
		$this->load->view("backend/login",$data);
		$this->load->view("footer");
	 }
	 
	 //verifica si el usuario esta activo en la base de datos
	 public function logueado()
	 {
		 $usuario = $this->input->post('usuario');
		 $password = $this->input->post('password');
		 $this->load->view("head"); 
		 if (($usuario != NULL) &&($password != NUll))
		 {
			$consulta = $this->Us_mdl->login($usuario,$password);
		 	if ($consulta == FALSE)
		 	{
				$this->session->sess_destroy();
				$array_log= array("mensaje"=>"Login incorrecto - intentelo nuevamente"); 
			    $this->session->set_userdata($array_log);
			 	//redirect('seguridad/logueado');
			 	$this->load->view("backend/login",$data);
				$this->load->view("footer");
		 	}
		 	else
		 	{
				$array_log =  array('us' => $usuario, "pas"=>$password, "act" => TRUE);
		 		$this->session->set_userdata($array_log);
				redirect('administracion/');
		 	} 
		 } 
		 else
		 {
			 redirect('seguridad/');			 
		 }		 		 
		 
	 }	 
	
 }
 
 ?>
<?php
/**********************************************
 ***** Controlador de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 /**
  *  Controlador para el manejo de Servicios
  */
 class Servicios extends CI_Controller 
 {
     
     function __construct() 
     {
         parent::__construct();
		 $this->load->library('botdetect/BotDetectCaptcha', array(
    	'captchaConfig' => 'Captcha' ));
		$this->load->library('email');
		
		 $this->load->helper("galeria_helper");
     }
	 
	 private function envio_mail($email="", $asunto="", $contenido="", $code="")
	 {
	 	
	 	if ($email != "") 
	 	{
			//envio de mail
			$this->email->from('info@urielpropiedades.com.ar', 'Ariel Marcelo Diaz');
			$this->email->to($email);
			$this->email->cc("marcelodiaz96@gmail.com");
			$this->email->bcc('sraulcantero@gmail.com');
			$this->email->subject($asunto);
			$this->email->message($contenido);
			$isHuman = $this->botdetectcaptcha->Validate($code);
			if ($isHuman)
			{
				if($this->email->send())
				{
					$data["mensaje"] = "mensaje enviado correctamente"; 
					return TRUE;
				}
				else
		    	{
					$data["mensaje"] = "Error en el envio de mensaje";
					return FALSE;
				}  			 
			} 
			else 
			{
  				$data["mensaje"] = "error en el codigo ingresado";  
				return FALSE;
	    	} 
		}
		 		 
	 }
	 
	 
	 
	 //descripcion de servicios
	 public function index()
	 {
		 $this->load->view("head");
         $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si'");
         $this->load->view("head-main",$data2);
         $this->load->view("frontend/servicios");
         $this->load->view("footer");
	 }
	 
	 //descripcion de consultoria inmobiliaria
	 public function consultoria()
	 {
		 $this->load->view("head");
         $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si'");
         $this->load->view("head-main",$data2);
         $this->load->view("frontend/servicios");
         $this->load->view("footer");
	 }
	 
	 //descripcion de tasaciones
	 public function tasaciones()
	 {
		 $this->load->view("head");
         $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si'");
         $this->load->view("head-main",$data2);	 
		 
		 // make Captcha Html accessible to View code
  		$data['captchaHtml'] = $this->botdetectcaptcha->Html();  		 
		$data["mensaje"] = "";
		 //carga de datos
		$email = $this->input->post("email");
		$asunto = $this->input->post("asunto");		 	
		$contenido = "Datos del correo:\n".
		                    "Apellido:".$this->input->post('apellido')."\n".
							"Nombre:".$this->input->post('nombre')."\n".
							"Email:".$this->input->post('email')."\n".
							"Telefono:".$this->input->post('telefono')."\n".
							"Tipo de inmueble:".$this->input->post('tipo-inmueble')."\n".
							"Antiguedad:".$this->input->post('antiguedad')."\n".
							"Mt2 descubiertos:".$this->input->post('mt2_descubiertos')."\n".
							"Mt2 cubiertos:".$this->input->post('mt2_cubiertos')."\n".
							"Mt2 semicubiertos:".$this->input->post('mt2_semicubiertos')."\n".
							"Mt2 totales:".$this->input->post('mt2')."\n".			
							"Cantidad de hambientes:".$this->input->post('ambientes')."\n".	
							"Estado de conservacion:".$this->input->post('conservacion')."\n".
							"Calidad de construccion:".$this->input->post('calidad-const')."\n".
							"Cocheras:".$this->input->post('cocheras')."\n".										
							"Mensaje:".$this->input->post('mensaje')."\n" ;		 
		 $code=$this->input->post("CaptchaCode");
		 
		 $this->envio_mail($email, $asunto, $contenido, $code); 
		 
         $this->load->view("frontend/tasaciones",$data);
         $this->load->view("footer");
	 }
	 
	 //descripcion de administracion
	 public function administracion()
	 {
		 $this->load->view("head");
         $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si'");
         $this->load->view("head-main",$data2);
         $this->load->view("frontend/administracion");
         $this->load->view("footer");
	 }
	 
	 //realiza la carga de datos de oportunidad laboral
	 public function oportunidad_laboral()
	 {
		$this->load->view("head");
        $data2["lista"] = $this->Propiedades_mdl->consulta("portada='si'");
        $this->load->view("head-main",$data2);		 
  		// make Captcha Html accessible to View code
  		$data['captchaHtml'] = $this->botdetectcaptcha->Html();  		 
		$data["mensaje"] = "";
		//carga de datos
		$email = $this->input->post("email");
		$asunto = $this->input->post("asunto");		 	
		$contenido = "Datos del correo:\n".
		                    "Apellido:".$this->input->post('apellido')."\n".
							"Nombre:".$this->input->post('nombre')."\n".
							"Email:".$this->input->post('email')."\n".
							"Telefono:".$this->input->post('telefono')."\n".
							"Direccion:".$this->input->post('direccion')."\n".	
							"Dispone de vehiculo:".$this->input->post('vehiculo')."\n".						
							"Mensaje:".$this->input->post('mensaje')."\n" ;		 
		 $code=$this->input->post("CaptchaCode");
		 
		$this->envio_mail($email, $asunto, $contenido, $code); 
	    $this->load->view("frontend/oportunidad-laboral",$data);
        $this->load->view("footer"); 
	 }
 }
 
 ?>
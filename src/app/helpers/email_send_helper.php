<?php
/**********************************************
 ***** Helper *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 
 	if ( ! defined('BASEPATH')) exit('Acceso no permitido');
	
	if (!function_exists('enviar_email'))
	{
		function enviar_email($email="",$asunto="", $contenido="")
		{
			//envio de mail
			$this->email->from('info@urielpropiedades.com.ar', 'Ariel Marcelo Diaz');
			$this->email->to($email);
			$this->email->cc("marcelodiaz96@gmail.com");
			$this->email->bcc('sraulcantero@gmail.com');
			$this->email->subject($asunto);
			$this->email->message($contenido);
			return $this->email->send();
		}
	}
 ?>

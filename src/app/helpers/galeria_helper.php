<?php
/**********************************************
 ***** Helper *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
	if ( ! defined('BASEPATH')) exit('Acceso no permitido');
	
//si no existe la función invierte_date_time la creamos
if(!function_exists('genera_galeria'))
{    
	function genera_galeria($array)
	{		
		$cant = count($array);
		$ini = "<!-- Carousel
    ================================================== -->
    <div id='myCarousel' class='carousel slide' data-ride='carousel'>
      <!-- Indicators -->
      <ol class='carousel-indicators'> \n";
		if ($cant < 6)
		{
			for ($i=0; $i < $cant ; $i++)
			{ 
				
				if ($i==0) 
				{
					$ini .= "<li data-target='#myCarousel' data-slide-to='".$i."' class='active'></li>\n ";
				} 
				else 
				{
					$ini .= "<li data-target='#myCarousel' data-slide-to='".$i."'></li>\n ";
				}
				
			}
		} 
		else 
		{
			for ($i=0; $i < 6 ; $i++)
			{ 
				if ($i==0) 
				{
					$ini .= "<li data-target='#myCarousel' data-slide-to='".$i."' class='active'></li>\n ";
				} 
				else 
				{
					$ini .= "<li data-target='#myCarousel' data-slide-to='".$i."'></li>\n ";
				}
			}
		}
		$ini .= "</ol>
      <div class='carousel-inner' role='listbox'>\n";		
		
	  
	  $fin = "</div>
      <a class='left carousel-control' href='#myCarousel' role='button' data-slide='prev'>
        <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
      </a>
      <a class='right carousel-control' href='#myCarousel' role='button' data-slide='next'>
        <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
      </a></div>
    <!-- /.carousel -->";
    	$i=0; $media = "";    	
 	 	foreach ($array as $row)
		{ // generar cantidad
			if ($i==0)
			{
				$media .= "<div class='item active'>
        					  <img src='".base_url()."media/img/portada/portada".
          						$row->banner."' alt='".$row->nombre."'>
          <div class='container'>
            <div class='carousel-caption'>
              <h1>".$row->nombre."</h1>
              <p>".$row->descripcion."</p>
              <p><a class='btn btn-lg btn-primary' href='".
              		base_url()."index.php/propiedades/detalle/".$row->idProp.
              		"' role='button'>Ver Detalles</a></p>
            </div>
          </div>
        </div>";
			} 
			else
			{
				$media .= "<div class='item'>
          <img  src='".base_url()."media/img/portada/portada".
          				$row->banner."' alt='".$row->nombre."'>
          <div class='container'>
            <div class='carousel-caption'>
              <h1>".$row->nombre."</h1>
              <p>".$row->descripcion."</p>
              <p><a class='btn btn-lg btn-primary' href='".
              		base_url()."index.php/propiedades/detalle/".$row->idProp.
              		"' role='button'>Ver Detalles</a></p>
            </div>
          </div>
        </div>";
			}
			$i++;	 
		}
		
		return  $ini.$media.$fin;
	}
}

if(!function_exists('genera_galeria_propiedad'))
{
	function genera_galeria_propiedad($array)
	{
		$i=0;		
		$ini = "<!-- Carousel
    ================================================== -->
    <div id='myCarousel' class='carousel slide' data-ride='carousel'>
      <!-- Indicators -->
      <ol class='carousel-indicators'>\n ";
      
      	if (!isset($row->imagenes1)) 
      	{
			$ini .= "<li data-target='#myCarousel' data-slide-to='".$i."' class='active'></li>\n"; 
			$i++;
		}
		
		if (!isset($row->imagenes2)) 
      	{
			$ini .= "<li data-target='#myCarousel' data-slide-to='".$i."' ></li>\n"; 
			$i++;
		}
		
		if (!isset($row->imagenes3)) 
      	{
			$ini .= "<li data-target='#myCarousel' data-slide-to='".$i."' ></li>\n"; 
			$i++;
		}
		
		if (!isset($row->imagenes4)) 
      	{
			$ini .= "<li data-target='#myCarousel' data-slide-to='".$i."' ></li>\n"; 
			$i++;
		}
		
		if (!isset($row->imagenes5)) 
      	{
			$ini .= "<li data-target='#myCarousel' data-slide-to='".$i."' ></li>\n"; 
			$i++;
		}
				      
		$ini .= "</ol>
      			<div class='carousel-inner' role='listbox'>";
	  
	  $fin = "</div>
      <a class='left carousel-control' href='#myCarousel' role='button' data-slide='prev'>
        <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
      </a>
      <a class='right carousel-control' href='#myCarousel' role='button' data-slide='next'>
        <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
      </a>
    </div><!-- /.carousel -->";
    	$media = "";
    	foreach ($array as $row)
    	{    		
    		
    		if($row->imagenes1 != "")
    		{
				$media .= "<div class='item active'>
         		 <img  src='".base_url()."media/img/".$row->idProp."/".
          			$row->idProp.$row->imagenes1."' alt='".$row->nombre."'>
          		
        		</div>";
			}
			
			if($row->imagenes2 != "")
    		{
				$media .= "<div class='item'>
         		 <img  src='".base_url()."media/img/".$row->idProp."/".
          			$row->idProp.$row->imagenes2."' alt='".$row->nombre."'>
          		
        		</div>";
			}			
			
			
			if($row->imagenes3 != "")
    		{
				$media .= "<div class='item'>
         		 <img  src='".base_url()."media/img/".$row->idProp."/".
          			$row->idProp.$row->imagenes3."' alt='".$row->nombre."'>
          		
        		</div>";
			}
			
			if($row->imagenes4 != "")
    		{
				$media .= "<div class='item'>
         		 <img  src='".base_url()."media/img/".$row->idProp."/".
          			$row->idProp.$row->imagenes4."' alt='".$row->nombre."'>
          		
        		</div>";
			}
			
			if($row->imagenes5 != "")
    		{
				$media .= "<div class='item'>
         		 <img  src='".base_url()."media/img/".$row->idProp."/".
          			$row->idProp.$row->imagenes5."' alt='".$row->nombre."'>
          		
        		</div>";
			}
		}
		return  $ini.$media.$fin;
	}
}	
<?php
/**********************************************
 ***** Helper *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
	if ( ! defined('BASEPATH')) exit('Acceso no permitido');
	
if (!function_exists('genera_random'))
{
	function genera_random()
	{		
		if (!isset($_SESSION["tiempo"]))
		{
			$_SESSION["tiempo"]= time();
			$_SESSION["ip"] = $this->input->ip_address();
			$string = substr(md5(rand()*time()),0,5);		
			$string = strtoupper($string);
			$string = str_replace("O","B", $string);
			$string = str_replace("0","C", $string);
			$_SESSION["tmptxt"] = $string;
		}
		
		if ($_SESSION["tiempo"]+20 >= time())
		{
			$string = substr(md5(rand()*time()),0,5);		
			$string = strtoupper($string);
			$string = str_replace("O","B", $string);
			$string = str_replace("0","C", $string);
			return $_SESSION["tmptxt"] = $string;
		}
		return $_SESSION["tmptxt"];				
    }
}
	
?>
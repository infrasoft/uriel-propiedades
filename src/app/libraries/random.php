<?php
/**********************************************
 ***** Controlador de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
/**
 *  Clase para la generacion de aleatorio
 */
class Random
{
		
	public function genera()
	{
		#generate a random string of 5 characters
		$string = substr(md5(rand()*time()),0,5);
	
		#make string uppercase and replace "O" and "0" to avoid mistakes
		$string = strtoupper($string);
		$string = str_replace("O","B", $string);
		$string = str_replace("0","C", $string);
		return $string;
	}
}
 
 ?>
<?php
/**********************************************
 ***** Modelo *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 /**
  * Clase para el manejo de comentarios
  */
 class Comentarios_mdl extends CI_Model 
 {
    private $tabla="comentarios"; 
     function __construct()
	 {
         parent::__construct();
     }
	 
	 //realiza la insersion de un nuevo comentario
	 public function nuevo($data=array())
	 {
		 return $this->db->insert($this->tabla, $data);
	 }
	 
	 //realiza la consulta de la tabla
	 public function consulta($data='')
	 {
		 $this->db->select("*");
		$this->db->from($this->tabla);
		
		if ($data!= "")
		{
			$this->db->where($data);
		}
		else			
	    {
			$this->db->where("1 ORDER BY idProp desc");
	    }
		$query = $this->db->get();
		 //$query->row(); 
		return $query->result();
	 }
	 
	 //modifica los datos del comentario
	 public function modifica($idprop=0,$data=array())// hay que corregir
	 {
		 $this->db->where('idProp', $idprop);
		 return $this->db->update($this->tabla, $data);
	 }
 }
 
 
 ?>
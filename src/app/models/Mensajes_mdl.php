<?php
/**********************************************
 ***** Modelo *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 /**
  * Modelos de Mensajes 
  */
 class Mensajes_mdl extends CI_Model
 {
     private $tabla="mjes";
     function __construct()
	 {
         parent::__construct();
     }
	 
	 //inserta una nuevo mensaje
	 public function nueva($data)
	 {
		 return $this->db->insert($this->tabla, $data);
	 }
	 
	 //modifica los elementos del modelo
	 public function modifica($id_mensaje=0, $data= array())
	 {
		 $this->db->where('id_mensaje', $idprop);
		 return $this->db->update($this->tabla, $data);
	 }
	 
	 //realiza la consulta del modelo
	 public function consulta($data) 
	 {
		 $this->db->select("*");
		$this->db->from($this->tabla);
		
		if ($data!= "")
		{
			$this->db->where($data);
		}
		else			
	    {
			$this->db->where("1 ORDER BY id_mensaje desc");
	    }
		$query = $this->db->get();
		 //$query->row(); 
		return $query->result();
	 }
 }
 
 ?>
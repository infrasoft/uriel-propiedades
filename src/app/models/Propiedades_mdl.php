<?php
/**********************************************
 ***** Modelo *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 /**
  *  Modelo de propiedades 
  */
 class Propiedades_mdl extends CI_Model
{
     private $tabla="propiedad";
     function __construct()
	 {
         parent::__construct();
     }
	 
	 //inserta una nueva propiedad
	 public function nueva($data)
	 {
		 return $this->db->insert($this->tabla, $data);
	 }
	 
	 //modifica los elementos de una propiedad
	 public function modifica($idprop=0, $data= array())
	 {
		 $this->db->where('idProp', $idprop);
		 return $this->db->update($this->tabla, $data);
	 }
	 
	 //realiza la consulta del modelo
	 public function consulta($data) //probar
	 {
		 $this->db->select("*");
		$this->db->from($this->tabla);
		
		if ($data!= "")
		{
			$this->db->where($data);
		}
		else			
	    {
			$this->db->where("1 ORDER BY idProp desc");
	    }
		$query = $this->db->get();
		 //$query->row(); 
		return $query->result();
	 }
	 
	 //realiza una consulta y devuelve la cantidad de elementos
	 public function nro_consulta($data='')
	 {
		$this->db->select("*");
		$this->db->from($this->tabla);
		
		if ($data!= "")
		{
			$this->db->where($data);
		}		
		$query = $this->db->get();
		 //$query->row(); 
		return $query->num_rows(); 
	 }
 }
 
 ?>
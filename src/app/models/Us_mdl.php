<?php
/**********************************************
 ***** Modelo *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 /**
  *  Modelo de usuarios 
  */
  
  class Us_mdl extends CI_Model
  {
      
      function __construct()
	  {
          parent::__construct();		  
      }
	  
	  public function login($user='',$pass ='') //falta encriptar el password
	  {	//seguridad
	  	  $user = $this->security->xss_clean($user);
		  $pass = $this->security->xss_clean($pass);
		  
	  	  $this->db->where(array('user' =>$user, 'pass'=>$pass));		  
		  $query = $this->db->get('us');
		  if($query->num_rows() == 1)
		  {
			return $query->row();
		  }
		  else
		  {				
			return FALSE;
		  }
	  }
	  
	  //controla la seccion del login
	  public function controltiempo()
	  {
		  
	  }
  }
  
 
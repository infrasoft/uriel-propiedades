<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<body>

<div class="brand">Uriel Propiedades</div>
    <div class="address-bar">
        <i class="fa fa-address-book" aria-hidden="true"></i>
        Alvarado 1073. Local 3| Salta Capital |
        <i class="fa fa-mobile" aria-hidden="true"></i> 3875671141 - 
        <i class="fa fa-mobile" aria-hidden="true"></i>3874416157 |
        <a href="https://www.facebook.com/UrielPropiedades/" alt="Fan Page de Uriel propiedades">
            <i class="fa fa-facebook-square" aria-hidden="true"></i>
        </a>
    </div>
            
        
        <div class="container panel">   
            <nav class="navbar navbar-default navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Uriel Propiedades</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">                
                
                <li>
                    <a href="<?=base_url();?>index.php/administracion/">
                        <i class="fa fa-hospital-o" aria-hidden="true"></i>
                        Propiedades
                     </a>
                </li>
                
                <li>
                  <a href="<?=base_url();?>index.php/administracion/listacomentarios/" >
                    <i class="fa fa-server" aria-hidden="true"></i>
                    Comentarios
                   </a>
                  
                </li>
                <li>
                    <a href="<?=base_url();?>index.php/administracion/lista_mensajes">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        Mensajes
                    </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
	




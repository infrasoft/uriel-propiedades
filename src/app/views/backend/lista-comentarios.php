<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<h1> Lista Comentarios</h1>
<div class="formulario">
	<!- Formulario de Busqueda -->
	<form class="form-inline" role="form">
	<input type="text" class="form-control"  placeholder="Filtrar Datos"/>
	<button type="submit" class="btn btn-primary">Buscar</button> 
	</form>
	
	<table class="table table-striped">
		<tr class="active">
			<td>id Prop</td>
			<td>id User</td>
			<td>fecha</td>
			<td>hora</td>
			<td>comentario</td>
			<td>OP</td>
		</tr>
		<?php 
			foreach ($datos as $row )
			{
				echo "<tr>
						<td>".$row->idProp."</td>
						<td>".$row->idUser."</td>
						<td>".$row->fecha."</td>
						<td>".$row->hora."</td>
						<td>".$row->comentario."</td>
						<td>".$row->estado."</td>
						<td><a href='".base_url().
							"index.php/administracion/modifica_comentario/".$row->estado.
							"'><span class='glyphicon glyphicon-pencil'></span></a></td>
					 </tr>";
			}
		?>
	</table>
</div>
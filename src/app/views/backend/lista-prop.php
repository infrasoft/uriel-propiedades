<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<div class="text-right">
	<a href="<?=base_url();?>index.php/administracion/nueva_prop">
		<button type="button" class="btn btn-default">Nueva propiedad</button>
	</a>
</div>

<div class="formulario">
<h1>Lista de propiedades</h1>
<?=form_open('', 
                  array('class' => "form-inline" , 'role' => "form",
                               'id'=>'buscar_propiedad', 'name' =>"buscar_propiedad"))?>
	<input type="text" class="form-control"  placeholder="Filtrar Datos" id="buscar" name="buscar"/>
	<button type="submit" class="btn btn-primary">Buscar</button> 
</form>

<br/>

<table class="table table-striped">
	<tr class="active">
		<td>id</td>
		<td>Nombre</td>
		<td>Descripcion</td>
		<td>img</td>
		<td>Estado</td>
		<td>OP</td>
	</tr>
	<?php
		//print_r($datos);		
		foreach ($datos as $row) 
		{
			echo "<tr>
						<td>".$row->idProp."</td>
						<td>".$row->nombre."</td>
						<td>".$row->descripcion."</td>
						<td><img href='".base_url()."media/img/portada/portada".$row->banner."' class='img-peque'></td>
						<td>".$row->estado."</td>
						<td>
							<a href='".base_url()."index.php/administracion/modifica_prop/"
									.$row->idProp."/' alt='Modificar datos de la propiedad'>
								<i class='fa fa-pencil-square-o' aria-hidden='true'></i>
							</a></td>
				  </tr>";	
		}
	?>
</table>
</div>
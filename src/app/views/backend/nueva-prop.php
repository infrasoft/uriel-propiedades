<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<h1><?=$titulo;?></h1>
<?=$mensaje;
	if ($titulo == "Modificar datos de propiedad")
	{
		$consulta = $this->Propiedades_mdl->consulta("idProp=".$id);
		foreach ($consulta as $row)
		{
		    $nombre = $row->nombre;
			$descripcion = $row->descripcion;
			$localidad = $row->localidad;
			$direccion = $row->direccion;
			$tipo = $row->tipo;
			$operacion = $row->operacion;
			$requisitos = $row->requisitos;
			$documentacion = $row->documentacion;
			$precio = $row->precio;
			$planos = $row->planos;
			$agua = $row->agua;
			$luz = $row->luz;
			$gaz = $row->gaz;
			$cloacas = $row->cloacas;
			$asfalto = $row->asfalto;
			$sup_construida = $row->sup_construida;
			$sup_semidescubierta = $row->sup_semidescubierta;
			$sup_descubierta = $row->sup_descubierta;
			$sup_total = $row->sup_total;
			$banner  = $row->banner ;
			$imagenes1  = $row->imagenes1 ;
			$imagenes2  = $row->imagenes2 ;
			$imagenes3  = $row->imagenes3 ;
			$imagenes4  = $row->imagenes4 ;
			$imagenes5  = $row->imagenes5 ;
			$mapa  = $row->mapa ;
			$estado  = $row->estado ;
			$otros  = $row->otros ;
		}
	} 
	else
   	{//nueva propiedad
		$nombre = "";
		$descripcion = "";
		$localidad = "";
		$direccion = "";
		$tipo = "";
		$operacion = "";
		$requisitos = "";
		$documentacion = "";
		$precio = "";
		$planos = "";
		$agua = "";
		$luz = "";
		$gaz = "";
		$cloacas = "";
		$asfalto = "";
		$sup_construida = "";
		$sup_semidescubierta = "";
		$sup_descubierta = "";
		$sup_total = "";
		$banner = "";
		$imagenes1 = "";
		$imagenes2 = "";
		$imagenes3 = "";
		$imagenes4 = "";
		$imagenes5 = "";
		$mapa = "";
		$estado = "";
		$otros = "";
	}
		
?>
<div class="formulario">
	
	<?=form_open_multipart('', 
                  array('class' => "form-inline" , 'role' => "form",
                               'id'=>'nueva_prop', 'name' =>"nueva_prop")) ?>
		<div class="form-group">
			<label class="sr-only" for="apellido"> Nombre</label>
			<input type="name" class="form-control" placeholder="Nombre" 
				required="Se requiere el ingreso del Nombre"
				name="nombre" id="nombre" value="<?=$nombre;?>"/>
				
			<textarea class="form-control" placeholder="Descripcion" 
				id="descripcion" name="descripcion" id="descripcion" 
				 required><?=$descripcion;?></textarea>
		</div>
		<br />
		<div class="form-group">	
			<label>Provincia</label>
			<select class="form-control" id="provincia" name="provincia">
				<option>Salta</option>
			</select>
			
			<label class="sr-only" for="direccion"> Localidad</label>
			<input type="text" class="form-control" placeholder="Localidad"
				name="localidad" id="localidad" value="<?=$localidad;?>" required/>
				
			<label class="sr-only" for="direccion"> Direccion</label>
			<input type="text" class="form-control" placeholder="Direccion"
				name="direccion" id="direccion" value="<?=$direccion;?>" required/>
		</div>
		<br />
		<div class="form-group">		
			<label>Tipo de propiedad</label>
			<select class="form-control" id="tipo" name="tipo" >
				<option <?php if ($tipo =="casa"){echo "selected";} ?>>casa</option>
				<option <?php if ($tipo =="cochera"){echo "selected";} ?>>cochera</option>
				<option <?php if ($tipo =="departamento"){echo "selected";} ?>>departamento</option>
				<option <?php if ($tipo =="edificio"){echo "selected";} ?>>edificio</option>
				<option <?php if ($tipo =="galpon"){echo "selected";} ?>>galpon</option>
				<option <?php if ($tipo =="local"){echo "selected";} ?>>local</option>
				<option <?php if ($tipo =="quinta"){echo "selected";} ?>>quinta</option>
				<option <?php if ($tipo =="terreno"){echo "selected";} ?>>terreno</option>				
				
			</select>
			
			<label>Operacion</label>
			<select class="form-control" id="operacion" name="operacion">
				<option <?php if ($operacion =="venta"){echo "selected";} ?>>venta</option>
				<option <?php if ($operacion =="alquiler"){echo "selected";} ?>>alquiler</option>
				<option <?php if ($operacion =="tasacion"){echo "selected";} ?>>tasacion</option>
				<option <?php if ($operacion =="administracion"){echo "selected";} ?>>administracion</option>
				
			</select>
		</div>
		<br />
		<div class="form-group">	
			<label class="sr-only" for="direccion"> Requisitos</label>
			<input type="text" class="form-control" placeholder="Requisitos"
				name="requisitos" id="requisitos"  value="<?=$requisitos;?>" required/>
			
			<label class="sr-only" for="direccion"> Documentacion</label>
			<input type="text" class="form-control" placeholder="Documentacion"
				name="documentacion" id="documentacion" value="<?=$documentacion;?>" required/>			
		</div>
		<br />
		<div class="form-group">	
			<div class="input-group">
				<label class="sr-only" for="direccion"> Precio</label>
  				<span class="input-group-addon">$</span>
  				<input type="number" class="form-control" placeholder="Precio"
  				id="precio" name="precio" value="<?=$precio;?>" required>
  				<span class="input-group-addon">.00</span>
			</div>	
			
			<label class="sr-only" for="direccion"> Planos</label>
			<input type="text" class="form-control" placeholder="Planos"
				name="planos" id="planos" value="<?=$planos;?>" />	
		</div>
		<br />
		<div class="form-group">		
			<div class="checkbox">
  				<label>
    				<input type="checkbox" value="si" id="agua" name="agua"
    				 <?php if ($agua =="si"){echo "checked";} ?> />
    					agua
  				</label> -
  				<label>
    				<input type="checkbox" value="si" id="luz" name="luz" 
    				  <?php if ($luz =="si"){echo "checked";} ?>/>
    					luz
  				</label> -
  				<label>
    				<input type="checkbox" value="si" id="gaz" name="gaz"
    				  <?php if ($gaz =="si"){echo "checked";} ?> />
    					gaz
  				</label> -
  				<label>
    				<input type="checkbox" value="si" id="cloacas" name="cloacas" 
    				 <?php if ($cloacas =="si"){echo "checked";} ?>/>
    					Cloacas
  				</label> -
  				<label>
    				<input type="checkbox" value="si" id="asfalto" name="asfalto" 
    				  <?php if ($asfalto =="si"){echo "checked";} ?> />
    					Asfalto
  				</label>
			</div>
		</div>
		<br />
		<div class="form-group">
			
			<label class="sr-only"> Sup Construida</label>
			<div class="input-group">				
  				<span class="input-group-addon">mt2</span>
  				<input type="number" class="form-control" placeholder="Sup Construida"
  				id="sup_construida" name="sup_construida" value="<?=$sup_construida;?>" >
  				<span class="input-group-addon">.00</span>
			</div>
			<br />
			<label class="sr-only"> Sup Semi Cubierta</label>
			<div class="input-group">				
  				<span class="input-group-addon">mt2</span>
  				<input type="number" class="form-control" placeholder="Sup Semi Cubierta"
  				id="sup_semidescubierta" name="sup_semidescubierta" value="<?=$sup_semidescubierta;?>" >
  				<span class="input-group-addon">.00</span>
			</div>
			<br />
			<label class="sr-only"> Sup Descubierta</label>
			<div class="input-group">				
  				<span class="input-group-addon">mt2</span>
  				<input type="number" class="form-control" placeholder="Sup Descubierta"
  				id="sup_descubierta" name="sup_descubierta" value="<?=$sup_descubierta;?>" >
  				<span class="input-group-addon">.00</span>
			</div>
			<br />
			<label class="sr-only"> Superficie Total</label>
			<div class="input-group">				
  				<span class="input-group-addon">mt2</span>
  				<input type="number" class="form-control" placeholder="Superficie Total"
  				id="sup_total" name="sup_total" value="<?=$sup_total;?>" required>
  				<span class="input-group-addon">.00</span>
			</div>	
		</div>
		<br />
		<div class="form-group">	
			<label  > Banner</label>
			<input type="file" class="form-control" placeholder="Banner"
				name="banner" id="banner" required />
		</div>
		<br />
		<div class="form-group">		
			<label  > Imagenes 1</label>
			<input type="file" class="form-control" placeholder="Imagenes"
				name="imagenes1" id="imagenes1" required />
			<br />	
			<label  > Imagenes 2</label>
			<input type="file" class="form-control" placeholder="Imagenes"
				name="imagenes2" id="imagenes2" />
			<br />		
			<label  > Imagenes 3</label>
			<input type="file" class="form-control" placeholder="Imagenes"
				name="imagenes3" id="imagenes3" />
			<br />		
			<label  > Imagenes 4</label>
			<input type="file" class="form-control" placeholder="Imagenes"
				name="imagenes4" id="imagenes4"  />
			<br />		
			<label  > Imagenes 5</label>
			<input type="file" class="form-control" placeholder="Imagenes"
				name="imagenes5" id="imagenes5"  />
		</div>
		<br />
		<div class="form-group">		
			<label class="sr-only" for="direccion"> Mapa</label>
			<input type="url" class="form-control" placeholder="Mapa"
				name="mapa" id="mapa" value="<?=$mapa;?>" />
			<br />			
			
			<textarea class="form-group" placeholder="Otros Datos" id="otros"
			 	name="otros"><?=$otros;?>
			 </textarea>
		</div>
		<br />
		<div class="form-group">
			<label>Estado</label>
			<select id="estado" name="estado">
				<option <?php if ($estado =="abierta"){echo "selected";} ?>>abierta</option>
				<option <?php if ($estado =="cerrada"){echo "selected";} ?>>cerrada</option>
			</select>
			<button type="submit" class="btn btn-primary">Enviar Datos</button>
		</div>
	</form>
</div>

<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<h1> Administracion de propiedades</h1>
<p>
	Los clientes eligen nuestros servicios de administración para su propiedad horizontal por 
	la amable y óptima atención que ofrece, ofrecemos a nuestros clientes los siguientes
	 servicios en la administración:
</p>
<ul>
<li><p>Nuestro equipo de trabajo mediara antes posibles conflictos</p></li>	
<li><p>Tanto propietarios como inquilinos recibirán un buen trato y mucha cordialidad, en todas 
	las situaciones que puedan presentarse.</p></li>
	
<li><p>Se enviaran al propietarios informes mensuales del pago y gastos generados en el inmueble.
</p></li>	
<li><p>Se realizaran balances anuales para el estudio y detalle del mismo.</p></li>	

<p> Dudas o consultas estamos a su disposicion</p>
<br /><br />


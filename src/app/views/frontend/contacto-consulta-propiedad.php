<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<h1>Contacto</h1>
<div class="formulario">
	<?php echo $nombre.form_open('', 
                  array('class' => "form-inline" , 'role' => "form",
                               'id'=>'contactoid', 'name' =>"contactoid"))
	?>
	<form class="form-inline" role="form" name="contacto" id="contacto">
		<div class="form-group">
			<input type="hidden" name="idProp" id="idProp" value="<?=$idProp;?>"/>
			<label class="sr-only" for="apellido"> Apellido</label>
			<input type="name" class="form-control" placeholder="Apellido" 
			required="Se requiere el ingreso del apellido"
			name="apellido" id="apellido" />
		</div>	
		<div class="form-group">
			<label class="sr-only" for="nombre"> Nombre</label>
			<input type="name" class="form-control" placeholder="Nombre" 
			required="Se requiere el ingreso del nombre"
			name="nombre" id="nombre" />
		</div>
		<br /><hr />
		<div class="input-group">	
			<span class="input-group-addon">@</span>
			<label class="sr-only" for="email"> Email</label>
			<input type="email" class="form-control" placeholder="Email" 
			required="Ingrese el email" name="email" id="email" />
		</div>
		
			
			<label class="sr-only" for="telefono"> Telefono</label>
			<input type="number" class="form-control" placeholder="Telefono"
			name="telefono" id="telefono" required/>
		
		<hr />
		<div class="form-group">	
			<label class="sr-only" for="direccion"> Direccion</label>
			<input type="text" class="form-control" placeholder="Direccion"
			name="direccion" id="direccion"  />
			
			<label class="sr-only" for="asunto"> Asunto</label>
			<input type="text" class="form-control" placeholder="Asunto" 
			name="asunto" id="asunto" />
		
		<hr />
		<textarea class="form-control" placeholder="Ingrese su mensaje"
		name="mensaje" id="mensaje" rows="3"></textarea>
		<br />		
		<img src="<?php echo base_url();?>index.php/captcha_image/" />
	    <input type="text" name="CaptchaCode" id="CaptchaCode" value="" 
	    class="form-control"  placeholder="Ingrese el captcha" required/>
		<button type="submit" class="btn btn-primary">
			Enviar
		</button>
		
		</div>
		
	</form>
	<p  class="text-muted"><small> (*) Algunos campos de este formulario son obligatorios</small></p>
	<?=$mensaje;?>
</div>
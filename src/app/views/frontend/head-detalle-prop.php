<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<body>
        <div class="brand">Uriel Propiedades</div>
    <div class="address-bar">
        <i class="fa fa-address-book" aria-hidden="true"></i>
        Alvarado 1073. Local 3| Salta Capital |
        <i class="fa fa-mobile" aria-hidden="true"></i> 3875671141 - 
        <i class="fa fa-mobile" aria-hidden="true"></i>3874416157 |
        <a href="https://www.facebook.com/UrielPropiedades/" alt="Fan Page de Uriel propiedades">
            <i class="fa fa-facebook-square" aria-hidden="true"></i>
        </a>
    </div>
            
        
        <div class="container panel">   
            <nav class="navbar navbar-default navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Uriel Propiedades</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                
                <li>
                    <a href="<?=base_url();?>index.php/institucional/">
                        <i class="fa fa-university" aria-hidden="true"></i>
                        Institucional
                    </a>
                </li>
                <li>
                    <a href="<?=base_url();?>index.php/propiedades/">
                        <i class="fa fa-hospital-o" aria-hidden="true"></i>
                        Propiedades
                     </a>
                </li>
                
                <li class="dropdown">
                  <a href="<?=base_url();?>index.php/servicios/" class="dropdown-toggle" 
                  	 data-toggle="dropdown" role="button" aria-haspopup="true" 
                  	 aria-expanded="false">
                    	<i class="fa fa-server" aria-hidden="true"></i>
                    	Servicios 
                   		<span class="caret"></span>
                   </a>
                  <ul class="dropdown-menu">
                    <li>
                    	<a href="<?=base_url();?>index.php/servicios/consultoria">
                    		Consultoria Inmobiliaria
                    	</a>
                    </li>
                    <li>
                    	<a href="<?=base_url();?>index.php/servicios/tasaciones">
                    		Tasaciones de propiedades
                    	</a>
                    </li>
                    <li>
                    	<a href="<?=base_url();?>index.php/servicios/administracion">
                    		Administracion de propiedades
                    	</a>
                    </li>
                    <li>
                    	<a href="<?=base_url();?>index.php/servicios/administracion">
                    		Oportunidad Laboral
                    	</a>
                    </li>
                  </ul>
                </li>
                <li>
                    <a href="<?=base_url();?>index.php/contacto/">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        Contacto
                    </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        
        <?=genera_galeria_propiedad($consulta) ?>
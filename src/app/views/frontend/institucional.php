<h1>Institucional</h1>
<h2>Mision:</h2>
<p>Brindar un servicio de asesoría inmobiliaria personalizada orientada a cuidar el patrimonio 
	de nuestros clientes, con ética y profesionalismo.</p>

<p>Proporcionar a nuestros clientes asesoría inmobiliaria profesional, personalizada e integral, 
	con ética y honestidad. Y Sobre todo confiable para satisfacer sus necesidades
</p>
<h2>Vision:</h2>
<p>Ser una empresa líder en el mercado y reconocida por lograr los mejores resultados para 
	nuestros clientes, actuando siempre con integridad en cada operación.</p>

<p>Ofreciendo siempre un servicio profesional y personalizado, orientado a cuidar el patrimonio 
de nuestros clientes, y lograr un desarrollo empresarial.</p>

<p>Satisfacer las necesidades de nuestros clientes al mismo tiempo que impulsamos nuestro
 crecimiento como inmobiliaria a nivel regional.</p>
 
<p>Superando las expectativas y lograr un crecimiento exponencial en el área.</p>

<h2>Valores:</h2>

	<h3>Transparencia</h3>
<p>Propugnamos y mantenemos un comportamiento honesto, íntegro y transparente en todas nuestras 
	actividades y hacia todos nuestros clientes.</p>

	<h3>Compromiso</h3>
<p>Perseveramos y entregamos todo nuestro esfuerzo y profesionalismo en cada una de las
	 actividades y tareas que asumimos, cumpliendo con los plazos y términos establecidos.

	<h3>Trabajo en equipo</h3>
<p>Fomentamos la participación de todos los integrantes de la empresa y una colaboración 
	efectiva alineada con un objetivo común.

	<h3>Eficiencia</h3>
<p>Maximizamos la calidad de nuestros resultados mediante la utilización óptima de nuestros
	 recursos.
	 
<h3>	Enfoque al cliente</h3>
<p>Dirigimos todas nuestras acciones a superar las expectativas de nuestros clientes.</p>


<h2> Objetivos:</h2>
<ul>
	<li>La satisfacción total de los requerimientos y necesidades de nuestros clientes al 
		entregarles productos y servicios de la mejor calidad.</li>
	<li>	La satisfacción y el bienestar de los integrantes de la organización.</li>
	<li>La más alta rentabilidad para los clientes, los inversionistas y accionistas de la 
		empresa. </li> 
</ul>

<hr />

<h2 class="text-center"> Nuestro Equipo</h2>
<div class="row">
	
	<div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?=base_url();?>media/img/ariel_diaz_foto.jpg" 
      alt="Foto Ariel Diaz" class="img-circle foto-catalogo">
      <div class="caption">
        <h3>Ariel Diaz</h3>
        <p> Co Fundador de Uriel Propiedades 
        	 </p>
        <p>
        	<a href="#" class="btn btn-primary" role="button">Contactar</a>        	
        </p>
      </div>
    </div>
  </div>
  
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?=base_url();?>media/img/raul_cantero_foto.jpg" 
      alt="Foto Raul Cantero" width="250px" height="250px" class="img-circle foto-catalogo">
      <div class="caption">
        <h3>Raul Cantero</h3>
        <p> Co Fundador de Uriel Propiedades  </p>
        <p>
        	<a href="#" class="btn btn-primary" role="button">Contactar</a>        	
        </p>      </div>
    </div>
  </div>
  
</div>
<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/


?>
<h1>Oportunidad Laboral</h1>
<p class="text-center">Se buscan vendedores inmobiliarios</p>
<div class="formulario">
	<?=form_open('', 
                  array('class' => "form-inline" , 'role' => "form",
                               'id'=>'oportunidad_laboral', 'name' =>"oportunidad_laboral"))?>
                               
        <div class="form-group">
			<label class="sr-only" for="apellido"> Apellido</label>
			<input type="name" class="form-control" placeholder="Apellido" 
			required="Se requiere el ingreso del apellido"
			name="apellido" id="apellido" />
		</div>	
		<div class="form-group">
			<label class="sr-only" for="nombre"> Nombre</label>
			<input type="name" class="form-control" placeholder="Nombre" 
			required="Se requiere el ingreso del nombre"
			name="nombre" id="nombre" />
		</div>
		<br />
		<div class="form-group">
		<div class="input-group">	
			<span class="input-group-addon">@</span>
			<label class="sr-only" for="email"> Email</label>
			<input type="email" class="form-control" placeholder="Email" 
			required="Ingrese el email" name="email" id="email" />
		</div>
		
			
			<label class="sr-only" for="telefono"> Telefono</label>
			<input type="number" class="form-control" placeholder="Telefono"
			name="telefono" id="telefono" required/>	
		</div>
		<br />
		<div class="form-group">
			<label>¿Dispone de vehiculo?</label>
			<label class="radio-inline"><input type="radio" name="vehiculo" id="vehiculo">Si</label>
			<label class="radio-inline"><input type="radio" name="vehiculo" id="vehiculo">No</label>
		</div>
		<br />
		<div class="form-group">	
			<label class="sr-only" for="direccion"> Direccion</label>
			<input type="text" class="form-control" placeholder="Direccion"
			name="direccion" id="direccion"  />
			
			<label class="sr-only" for="asunto"> Asunto</label>
			<input type="text" class="form-control" placeholder="Asunto" 
			name="asunto" id="asunto" value="Oportunidad Laboral"/>
		</div>
		<br /><hr />
		<div class="form-group">
		<textarea class="form-control" placeholder="Ingrese su CV"
		name="mensaje" id="mensaje" rows="3"></textarea><br />
		<img src="<?php echo base_url();?>index.php/captcha_image/" />
	   <input type="text" name="CaptchaCode" id="CaptchaCode" value="" class="form-control"  placeholder="Ingrese el captcha" required/>
		
		<button type="submit" class="btn btn-primary">Enviar</button></div>
	<?php echo form_close()."<p>".$mensaje."</p>"; ?>
</div>
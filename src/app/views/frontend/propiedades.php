<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<h1> Bienvenidos</h1>
<div class="formulario">
	<?=form_open('', 
                  array('class' => "form-inline" , 'role' => "form",
                               'id'=>'buscar_propiedad', 'name' =>"buscar_propiedad"))?>
        <input class="form-control" placeholder="Buscar" id="dato" name="dato"/>
        <button class="btn btn-primary" type="submit" id="buscar" name="buscar">
        	Buscar
        </button>
    <?=form_close();?>                         
</div>
<div class="row">
  <?php 
  	foreach ($consulta as $row) 
  	  {
		  echo "<div class=\"col-sm-6 col-md-4\">
    <div class=\"thumbnail\">
    	<a href='".base_url()."index.php/propiedades/detalle/".$row->idProp."'>    
      	<img src=\"".base_url()."media/img/portada/portada".$row->banner."\" alt=\"".$row->nombre."\">
      	</a>
      <div class=\"caption\">
        <h3 class='text-center'>".$row->nombre."</h3>
        <p>".substr($row->descripcion, 0, 30)."</p>
        <p class='text-right'>
        	<a href='".base_url()."index.php/propiedades/detalle/".$row->idProp."' 
        		 class=\"btn btn-primary\" role=\"button\">
        		Ver Detalles
        	</a>
        </p> 
      </div>
    </div>
  </div>";
	  }
	  
	  
  ?>	
 </div>
<div class="text-center">
	<ul class="pagination">
<?php   /*
	  if ($cantidad < 9)
	  {//hacer paginacion
	  	 $i=1;
	  	 echo "<li><a href='".base_url()."index.php/propiedades/detalle/".$i."'>&laquo;</a></li>\n";
		 for ($i=1; $i < $cantidad; $i=$i+9) 
		 { 
			 echo "<li><a href='".base_url()."index.php/propiedades/detalle/".$i."'>2</a></li>\n";
		 }
		 echo "<li><a href='".base_url()."index.php/propiedades/detalle/".$cantidad."'>&raquo;</a></li>\n";// ver lo de paginacion
		 
	  }*/
?>
</ul>
</div>


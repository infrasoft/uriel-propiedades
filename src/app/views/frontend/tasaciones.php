<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<h1> Tasacion de Propiedades</h1>
<p> Para su comodidad, hemos desarrollado un sistema automatizado para la tasacion de su propiedad
	este mismo solo realizara un estimado del costo de su vivienda</p>
	<div class="formulario">
		<form class="form-inline" role="form" name="tasacion" id="tasacion">
			<div class="form-group">
				
				<h3>Datos Personales</h3> 
				
				<label class="sr-only">Apellido</label>
				<input type="name" class="form-control" placeholder="Apellido" 
					required="Se requiere el ingreso del apellido"
					name="apellido" id="apellido" />
				
				<label class="sr-only" > Nombre</label>
				<input type="name" class="form-control" placeholder="Nombre" 
					required="Se requiere el ingreso del nombre"
					name="nombre" id="nombre" />
					
			    <div class="input-group">	
					<span class="input-group-addon">@</span>
					<label class="sr-only" for="email"> Email</label>
					<input type="email" class="form-control" placeholder="Email" 
					required="Ingrese el email" name="email" id="email" />
				</div>
				
				<label class="sr-only" for="telefono"> Telefono</label>
				<input type="number" class="form-control" placeholder="Telefono"
					name="telefono" id="telefono" />
			</div>
			<h3> Datos de la tasacion:</h3> 
			<div class="form-group">
				<label> Tipo de inmueble</label>
				<select class="form-control" id="tipo-inmueble" name="tipo-inmueble">
					<option>Casa</option>
					<option>Departamento</option>
					<option>Local Comercial</option>
					<option>Finca</option>
					<option>Terreno</option>
				</select>
				
				<label class="sr-only">Antiguedad</label>
				<input type="number" class="form-control" placeholder="Antiguedad"
					name="antiguedad" id="antiguedad" />
				<div />
				
				<div class="form-group">
				<label class="sr-only">Mt2 Descubiertos:</label>
				<input type="number" class="form-control" placeholder="Mt2 Descubiertos"
					name="mt2_descubiertos" id="mt2_descubiertos"/>
				
				<label class="sr-only">Mt2 Cubiertos:</label>
				<input type="number" class="form-control" placeholder="Mt2 Cubiertos"
					name="mt2_cubiertos" id="mt2_cubiertos"/>
					
				<label class="sr-only">Mt2 Semicubiertos:</label>
				<input type="number" class="form-control" placeholder="Mt2 Semicubiertos"
					name="mt2_semicubiertos" id="mt2_semicubiertos"/>
					
				<label class="sr-only">Mt2 Totales:</label>
				<input type="number" class="form-control" placeholder="Mt2 Totales"
					name="mt2" id="mt2" required/>
				</div>
				
				<div class="form-group">
					
					
			    	<label>Cantidad de ambientes</label>
			    	<input  type="number" class="form-control" placeholder="ambientes" ç
			    	    id="ambientes" name="ambientes"/>
			    
			    	<label>Estado de conservacion</label>
			    	<select class="form-control" id="conservacion" name="conservacion">
			    		<option>a reciclar</option>
			    		<option>regular</option>
			    		<option>bueno</option>
			    		<option>muy bueno</option>
			    	</select>
			    </div>
				
				<div class="form-group">
			    	<label>Calidad de construccion</label>
			    	<select class="form-control" id="calidad-const" name="calidad-const">
			    		<option>ladrillones</option>
			    		<option>ladrillos</option>
			    		<option>modulos pre-armados</option>
			    		<option>otros</option>
			    	</select>
			    
			    	<label>Cocheras</label>
			    	<select class="form-control" id="cocheras" name="cocheras">
			    		<option>1</option>
			    		<option>2</option>
			    		<option>3</option>			    	
			    	</select>
			    </div>
			    	<hr />
			    <div class="form-group">
			    	<textarea class="form-control" placeholder="Otros datos" 	name="mensaje" id="mensaje" rows="3"></textarea>
			    	<img src="<?php echo base_url();?>index.php/captcha_image/" />
				   	<input type="text" name="CaptchaCode" id="CaptchaCode" value="" class="form-control"  placeholder="Ingrese el captcha"/>
			    	<button type="submit" class="btn btn-primary">Enviar Datos</button>		
			    </div>		
		</form>
		<?php echo $mensaje; ?>
	</div>
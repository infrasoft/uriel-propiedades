<?php 
/**********************************************
 ***** Vsta de acceso *****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Uriel Propiedades - Compra - Ventas - Administracion de Propiedades</title>
        <meta name="description" content="Empresa de servicios inmobiliarios en 
            Salta capital, alquileres,compras y ventas, tasaciones y 
        administración de propiedades">
        <meta name="author" content="Ariel Marcelo Diaz">
        
        <!-- CSS de Bootstrap -->
        <link href="<?=base_url();?>media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?=base_url();?>media/css/styles.css" rel="stylesheet" media="screen">
        
        <!-- Custom CSS -->
        <link href="<?=base_url();?>media/css/business-casual.css" rel="stylesheet">
        
        <!-- fontawesome -->
        <link rel="stylesheet" href="<?=base_url();?>media/css/font-awesome.min.css">
        
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
       <![endif]-->
      
        <meta name="viewport" content="width=device-width; initial-scale=1.0">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

        
        <link rel='shortcut icon' type='image/x-icon' href='<?=base_url();?>media/img/fondo.ico'>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>